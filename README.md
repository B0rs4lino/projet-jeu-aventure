# Projet Jeu Aventure

## Cas erreur JSONObject

Consulter le fichier [Informations/dependance.md](./Informations/dependance.md)

## Déploiement
1. Lancer directement le fichier ```Secret Gift.jar```

ou

1. Se rendre dans le répertoire contenant le fichier
2. Exécuter ```java -jar "Secret Gift".jar```

> Attention :warning: Le fichier jar doit impérativement se trouver dans le même répertoire que les dossiers fr/ , en/ , sauvegarde/

## Compilation

Consulter le fichier [Informations/compilation.md](./Informations/compilation.md)

## Accéder à la documentation

La documentation du projet se situe dans le fichier : [Secret Gift/doc/index.html](Secret Gift/doc/index.html)

## Thème

Dans sa maison le jour de Noël, Joëlle doit découvrir le contenu d’un cadeau verrouillé à l’aide d’un
cadenas.
## Résumé du scénario

### Scénario
En rentrant chez elle, Joëlle découvre un cadeau verrouillé à l’aide d’un cadenas à huit chiffres dans
son hall d’entrée. Elle va devoir fouiller la maison et résoudre certaines énigmes pour trouver des
indices, dans le but d’ouvrir le cadenas et de découvrir le contenu de ce cadeau mystère.

### Scénario complet
Le jour de noël, Joëlle rentre chez elle et découvre un mystérieux cadeau dans son hall d’entrée, celui-
ci verrouillé par un cadenas avec un code composé de huit chiffres. Dans le but d’en découvrir son
contenu, Joëlle décide de retrouver ces chiffres à travers la maison : partant du hall d’entrée, elle ne
peut pas monter à l’étage tant qu’elle n’a pas découvert tous les chiffres du rez-de-chaussée. Joëlle,
avançant de salle en salle, s’aperçoit que certains membres de sa famille possèdent de mystérieux
coffres. Ses coffres peuvent s’ouvrirent qu’après avoir résolu la question que sa famille lui posera.
Joëlle possède un joker lui permettant de passer une question si celle-ci est trop compliquée. Après
avoir découvert les quatre premiers chiffres du rez-de-chaussée, Joëlle décide de s’aventurer à l’étage
pour trouver les quatre derniers chiffres. Enfin, après avoir résolues les énigmes restantes et obtenus
les chiffres restants, elle descend rapidement au hall d’entrée, où elle avait aperçu le mystérieux
cadeau, et entre les chiffres qu’elle a trouvés. Le cadeau s’ouvre alors et contient un vélo neuf.


### Lieux, éléments, personnages, énigmes, commandes

#### 13 zones :
- 6 zones au rez-de-chaussée : le hall d’entrée (contenant le coffre verrouillé), une salle
à manger, un salon, un couloir, un atelier et la cuisine. Une énigme est disponible
dans chaque salle sauf le couloir.
- 7 zones à l’étage : 3 couloirs, 3 chambres et une salle d’étude. Une énigme est
disponible dans chaque salle sauf le couloir.

#### 9 éléments : 
- 8 énigmes qui permettront de déverrouiller un coffre + le cadeau final à l’entrée
de la maison

#### Commandes :
- Navigations (haut, bas, gauche, droite) : Pour naviguer entre les salles
- Enigme : Afficher l’énigme de la salle en cours
- Téléportation : Le joueur peut se téléporter dans n’importe quelle pièce débloquée
- Coffre : Le joueur se dirige vers le coffre
- Interagir : Récupérer le contenu du coffre
- 1, 2, 3, 4 : Lorsqu’une énigme est posée, le joueur entre le numéro correspondant à sa
réponse
- Joker : Permet d’obtenir la réponse à une question (utilisable qu’une seule fois)
- Aide : Affiche la liste des commandes disponibles

### Déroulement d’une partie gagnante
Le joueur démarre la partie dans le hall d’entrée et interagit avec le cadeau verrouillé. Il apprend que
ce coffre est verrouillé par un code à huit chiffres. Le joueur décide de lancer la commande « aide »
qui lui permet de connaître les commandes disponibles : il apprend qu’il possède dans son inventaire
un téléporteur qui lui permet de se retourner à tout moment à la zone de départ. Il décide de partir
chercher les numéros et a le choix entre différentes pièces : le joueur se dirige dans la cuisine. Il y
trouve un coffre verrouillé et sa mère qui va lui poser une énigme : « Je suis dans la cuisine, lorsqu’on
me tourne la tête je pleure, qui suis-je ? ». Le joueur répond la proposition « Robinet » et le coffre
s’ouvre. Il récupère le numéro 8 à la position 1 et se dirige vers le hall d’entrée pour aller dans la salle
à manger. Le joueur rencontre le père qui va lui poser une énigme : « mon premier est un pronom
possessif, mon second se trouve chez le boulanger, mon tout est un arbre ». Il répond la proposition
« Sapin » et le coffre s’ouvre : le joueur récupère le numéro 7 à la position 2. Il se déplace vers le salon
où il rencontre le grand-père et son énigme : « Mon premier est le trou d’une aiguille, Mon second est
un poisson et Mon tout est un bébé animal ». Le joueur répond « Chaton » et déverrouille le coffre qui
contient le numéro 1 à la position 3. Ensuite, il se dirige vers l’atelier où il voit la grand-mère : « Je suis
une chambre, mon numéro est le suivant : x+5=7 ». Le joueur répond « 3 » mais se trompe. Il n’a plus
le droit à l’erreur, sinon le jeu est perdu. Il essaye le nombre « 2 », le coffre se déverrouille et contient
le nombre 4 à la position 4. Les membres de la famille disparaissent du rez-de-chaussée et le joueur
peut désormais monter à l’étage.
Le joueur se déplace dans le hall de gauche et accède à la chambre nord. Il y retrouve la mère qui lui
pose une question : « Quel mot se cache derrière ce code ? 13-1-13-1-14 ». Le joueur répond
« Maman » et déverrouille le coffre de la chambre nord. Celui-ci contient le nombre 6 à la position 5.
Le joueur décide de se rendre à la chambre sud où il y trouve le père avec l’énigme : « Je suis dans la
salle à manger, j’ai 4 pieds t on m’utilise pour se poser ». Le joueur répond « Chaise » et parvient à
ouvrir le coffre qui contient le nombre 2 à la position 6. Il se dirige ensuite dans le hall à gauche et va
dans la chambre Ouest. Le joueur retrouve la grand-mère : « Blanc comme neige, je suis né dans l’eau.
Mais j’y retourne, je disparais aussitôt. Qui suis-je ? ». Il choisit « le sel » et obtient le numéro 2 à la
position 7. Enfin, le joueur décide de s’aventurer dans la dernière pièce de la maison, le bureau. Il y
retrouve le grand-père avec la question : « Mon premier est un appareil qui sonne le matin. Mon
deuxième est un pronom personnel souvent équivalent à « nous ». Mon tout est le nom donné au soir
de Noël ». Le joueur choisit la réponse « Le réveillon » et parvient à déverrouiller le coffre qui contient
le numéro 1 à la position 8.
Pour gagner du temps, le joueur décide d’utiliser le téléporteur obtenu au début de l’aventure qui lui
permet de retourner au hall d’entrée. Il interagit avec le coffre verrouillé : le personnage entre les
numéros trouvés précédemment et parvient à déverrouiller le mystérieux cadeau. Un vélo neuf
apparaît alors et la partie se termine.

### Enigmes
- Mon premier est un pronom possessif, mon second se trouve chez le boulanger, mon tout est
un arbre
  - Sapin : Bonne réponse
  - Autre : Mauvaise réponse
- Mon premier est le trou d’une aiguille, Mon second est un poisson et Mon tout est un bébé
animal
  - Chaton : Bonne réponse
  - Autre : Mauvaise réponse
- Je suis une chambre, mon numéro est le suivant : x+5=7
  - x=2 : Bonne réponse
  - x !=2 : Mauvaise réponse
- Quel mot se cache derrière ce code ? 13-1-13-1-14
  - Maman : Bonne réponse
  - Autre : Mauvaise réponse
- Je suis dans la cuisine, lorsqu’on me tourne la tête je pleure, qui suis-je ?
  - Robinet : Bonne réponse
  - Autre : Mauvaise réponse
- Je suis dans la salle à manger, j’ai 4 pieds t on m’utilise pour se poser :
  - Chaise : Bonne réponse
  - Autre : Mauvaise réponse
- Blanc comme neige, je suis né dans l’eau. Mais j’y retourne, je disparais aussitôt. Qui suis-je ?
  - Le sel : Bonne réponse
  - Autre : Mauvaise réponse
- Mon premier est un appareil qui sonne le matin. Mon deuxième est un pronom personnel
souvent équivalent à « nous ». Mon tout est le nom donné au soir de Noël.
  - Le réveillon : Bonne réponse
  - Autre : Mauvaise réponse


## Différentes situations
### Situation gagnante
Le joueur a pu ouvrir le cadeau à l’entrée avec les indices obtenus en répondant aux différentes
énigmes avant la fin du temps imparti.
### Situation perdante
Le joueur commet plus d’une erreur en répondant aux énigmes ou s’il n’a pas su ouvrir le cadeau
avant la fin du temps imparti.
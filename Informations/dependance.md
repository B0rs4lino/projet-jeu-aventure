# Ajouter une dépendance

Si l'erreur JSONObject se produit, il faut ajouter la dépendance java-json.jar au projet !

Pour ce faire :

1. Téléchargez l'archive [java-json.jar](./../java-json.jar)
2. Ajoutez l'archive au projet

        (Eclipse) Clique droit sur le projet > Build Path > Configure Build Path...
3. Cliquez sur ```Add External JARs...```

![Dependance 1](./dependance1.png)

4. Choisissez l'archive ```java-json.jar``` téléchargée précédemment

![Dependance 2](./dependance2.png)

5. Appuyez sur Apply and Close

Normalement, la dépendance a correctement été ajouté à votre projet !
# Compilation

## Compilation depuis Eclipse

![Capture ecran Eclipse](./capture_eclipse.png)

Clique droit sur le projet > Export...

Sélectionnez Java > Runnable JAR file

![Capture export](./capture_export.png)

Launch configuration : La classe principale
Export destination : Chemin de sortie du JAR

Cliquez sur Finish
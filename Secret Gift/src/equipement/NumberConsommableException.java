package equipement;

public class NumberConsommableException extends Exception {
	
	/** Constructeur de l'exception du nombre de consommable
	 * @param m Le message d'erreur
	 */
	public NumberConsommableException(String m) {
		super(m);
	}
}

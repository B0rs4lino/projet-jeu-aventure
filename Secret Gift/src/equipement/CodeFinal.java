package equipement;

import java.util.Random;

import jeu.Commande;

public class CodeFinal extends Item {

	/**
	 * Numéro secret du code 
	 */
	private String code;
	public static final int LENGTH_CODE = 8;

	/**
	 * Constructeur du CodeFinal
	 */
	public CodeFinal() {
		super(1, "Code Final", Commande.CODE);
		code = "";
	}
	
	/** Constructeur du CodeFinal
	 * @param r Le code final
	 */
	public CodeFinal(String r) {
		super(1, "Code Final", Commande.CODE);
		code = r;
	}
	
	/** Ajoute un numéro au code
	 * @param n Le numéro à ajouter au code
	 */
	public void ajouterNumero(int n) {
		code += Integer.toString(n);
	}
	
	/** Renvoie le code trouvé jusqu'à présent
	 * @return Le code trouvé jusqu'à présent
	 */
	public String getCode() {
		return code;
	}
	
	/** Renvoie si le code a été trouvé entièrement
	 * @return Si le code a été trouvé entièrement
	 */
	public boolean codeTrouveEntierement() {
		return LENGTH_CODE - code.length() == 0;
	}
	
	/** Renvoie si le code a été trouvé à moitié
	 * @return Si le code a été trouvé à moitié
	 */
	public boolean codeTrouveMoitie() {
		return code.length() >= CodeFinal.LENGTH_CODE / 2;
	}
	
	/** Renvoie la chaîne de caractère d'un code aléatoire
	 * @return La chaîne de caractère d'un code aléatoire
	 */
	public static String randomCode() {
		String r = "";
		for (int i = 0; i < LENGTH_CODE; i++) r += new Random().nextInt(9);
		return r;
	}

}

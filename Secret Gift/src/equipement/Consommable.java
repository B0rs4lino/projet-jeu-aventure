package equipement;

import jeu.Commande;

public class Consommable extends Item {
	
	/**
	 * Capacité disponible d'un consommable 
	 */
	private int disponible;
	/**
	 * Capacité maximale d'un consommable
	 */
	private int maxCapacite;

	/** Constructeur d'un consommable
	 * @param i L'identifiant du consommable
	 * @param n Le nom du consommable
	 * @param a La commande du consommable
	 * @param c La capacité maximale du consommable
	 * @throws NumberConsommableException
	 */
	public Consommable(int i, String n, Commande a, int c) throws NumberConsommableException {
		super(i, n, a);
		if (c < 0) throw new NumberConsommableException("Le nombre max de consommables ne peut pas être inférieur à 0");
		disponible = 1;
		maxCapacite = c;
	}
	
	/** Constructeur d'un consommable
	 * @param i L'identifiant du consommable
	 * @param n Le nom du consommable
	 * @param a La commande du consommable
	 */
	public Consommable(int i, String n, Commande a) {
		super(i, n, a);
		disponible = 0;
		maxCapacite = 1;
	}
	
	/** Constructeur d'un consommable
	 * @param i L'identifiant du consommable
	 * @param n Le nom du consommable
	 * @param a La commande du consommable
	 * @param c La capacité maximale du consommable
	 * @throws NumberConsommableException
	 */
	public Consommable(int i, String n, Commande a, int d, int c) throws NumberConsommableException {
		this(i, n, a, c);
		if (d > c) throw new NumberConsommableException("Le nombre de consommable ne peut pas être supérieur à la capacité disponible");
		if (d < 0) throw new NumberConsommableException("Le nombre de consommable ne peut pas être inférieur à 0");
		if (c < 0) throw new NumberConsommableException("Le nombre max de consommables ne peut pas être inférieur à 0");
		disponible = d;
	}

	/** Récupère la capacité maximale du consommable
	 * @return La capacité maximale du consommable
	 */
	public int getMaxCapacite() {
		return maxCapacite;
	}

	/** Récupère la capacité disponible du consommable
	 * @return La capacité disponible du consommable
	 */
	public int getDisponible() {
		return disponible;
	}

	/** Renvoie si le consommable est disponible
	 * @return Si le consommable est disponible
	 */
	public boolean estDisponible() {
		return disponible > 0;
	}
	
	/** Consomme l'item
	 * @return Réduit le nombre de consommable disponible
	 */
	public boolean consommeItem() {
		if (disponible == 0) return false;
		disponible--;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " " + disponible + "/" + maxCapacite;
	}
}

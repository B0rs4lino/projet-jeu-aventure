package equipement;

import java.util.ArrayList;

public class Inventaire {
	/**
	 * Liste des items de l'inventaire 
	 */
	private ArrayList<Item> listeObjet;

	/**
	 * Le Constructeur de l'inventaire 
	 */
	public Inventaire() {
		super();
		this.listeObjet = new ArrayList<Item>();
	}
	
	/** Ajouter un item dans l'inventaire
	 * @param i L'item à ajouter
	 */
	public void ajouterItem(Item i) {
		this.listeObjet.add(i);
	}
	
	/** Ajouter une liste d'items dans l'inventaire
	 * @param listeItem La liste d'item dans l'inventaire
	 */
	public void ajouterListeItem(ArrayList<Item> listeItem) {
		for (Item i : listeItem) {
			if (!listeObjet.contains(i)) listeObjet.add(i);
		}
	}
	
	/** Écraser la liste d'items de l'inventaire par une autre
	 * @param listeObjet Une liste d'items
	 */
	public void setListeObjet(ArrayList<Item> listeObjet) {
		this.listeObjet = listeObjet;
	}

	/** Renvoie la liste des items
	 * @return La liste des items
	 */
	public ArrayList<Item> getListe() {
		return this.listeObjet;
	}

	/** Renvoie une chaine de caractère du contenu de l'inventaire
	 * @return Une chaine de caractère du contenu de l'inventaire
	 */
	public String listeInventaire() {
		String r = "Liste des objets de l'inventaire :\n";
		if (listeObjet.size() == 0) r += "L'inventaire est vide.\n";
		else for (Item item : this.listeObjet) r += item.toString() + "\n";
		return r;
	}
	
	/** Vérifie si un consommable est disponible
	 * @param identifiant L'identifiant du consommable
	 * @return Renvoie si le consommable est disponible
	 */
	public boolean consommableDisponible(int identifiant) {
		for (Item i : listeObjet) {
			if (i.getIdentifiant() == identifiant && i instanceof Consommable) {
				return ((Consommable) i).estDisponible();
			}
		}
		return false;
	}
	
	/** Utilise le consommable dans l'inventaire
	 * @param identifiant L'identifiant du consommable
	 * @return Renvoie si le consommable a bien été utilisé, s'il n'y en a plus ou s'il est absent de l'inventaire
	 */
	public int utiliserConsommable(int identifiant) {
		for (Item i : listeObjet) {
			if (i.getIdentifiant() == identifiant && i instanceof Consommable) {
				return ((Consommable) i).consommeItem() ? 1 : 0;
			}
		}
		return -1;
	}
}

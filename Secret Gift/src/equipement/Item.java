package equipement;

import jeu.Commande;

public class Item {
	/**
	 * Identifiant unique de l'item
	 */
	private int identifiant;
	/**
	 * Nom de l'item
	 */
	private String nom;
	/**
	 * Commande pour utiliser l'item
	 */
	private Commande appel;
	
	/** Renvoie le nom de l'item
	 * @return Le nom de l'item
	 */
	public String getNom() {
		return nom;
	}

	/** Renvoie la commande de l'item
	 * @return La commande de l'item
	 */
	public Commande getAppel() {
		return appel;
	}

	/** Le constructeur de l'Item
	 * @param i L'identifiant de l'item
	 * @param n Le nom de l'item
	 * @param a La commande de l'item
	 */
	public Item(int i, String n, Commande a) {
		identifiant = i;
		nom = n;
		appel = a;
	}

	/** Renvoie l'identifiant de l'item
	 * @return L'identifiant de l'item
	 */
	public int getIdentifiant() {
		return identifiant;
	}

	@Override
	public String toString() {
		return this.nom + " -> " + this.appel.getAbreviation();
	}
	
	
}

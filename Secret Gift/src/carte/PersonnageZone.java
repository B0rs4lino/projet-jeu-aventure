package carte;

import java.util.ArrayList;

import jeu.Affichage;
import jeu.Affichage.LANGUE;

public class PersonnageZone {
	/** Renvoie si la première question a été répondu
	 * @return
	 */
	public boolean isEtage() {
		return etage;
	}

	/**
	 * Nom du personnage
	 */
	private String nom;
	/**
	 * Liste des énigmes
	 */
	private ArrayList<Enigme> listeEnigmes;
	/**
	 * Statut de la première énigme
	 */
	private boolean etage;

	/** Constructeur PersonnageZone
	 * @param nom Le nom du personnage
	 * @param enigme La liste des énigmes du personnage
	 */
	public PersonnageZone(String nom, ArrayList<Enigme> enigme) {
		super();
		this.etage = false;
		this.nom = nom;
		this.listeEnigmes = enigme;
	}
	
	/** Constructeur PersonnageZone
	 * @param nom Le nom du personnage
	 * @param enigme La liste des énigmes du personnage
	 * @param etage Si la première énigme a été répondu
	 */
	public PersonnageZone(String nom, ArrayList<Enigme> enigme, boolean etage) {
		this(nom, enigme);
		this.etage = etage;
	}
	
	/** Renvoie la chaine de caractère de l'énigme selon la langue
	 * @param l La langue choisie
	 * @return L'énigme selon la langue
	 */
	public String afficherEnigme(LANGUE l) {
		try {
			return enigmeCourante().afficher(l) + "\n" + Affichage.trouverTexteStatic("reponse", l);
		} catch (Exception e) {
			return enigmeCourante().afficher(LANGUE.FRANCAIS) + "\nRéponse: ";
		}
	}
	
	/**
	 * Considérer l'énigme comme résolue 
	 */
	public void setEnigmeResolue() {
		this.etage = true;
	}
	
	/** Renvoie le nom du personnage
	 * @return Le nom du personnage
	 */
	public String getNom() {
		return nom;
	}
	
	/** Renvoie la liste des énigmes du personnage
	 * @return La liste des énigmes du personnage
	 */
	public ArrayList<Enigme> getListeEnigmes() {
		return listeEnigmes;
	}
	
	/** Renvoie l'énigme courante
	 * @return L'énigme courante
	 */
	public Enigme enigmeCourante() {
		return !etage ? listeEnigmes.get(0) : listeEnigmes.get(1);
	}
	
	/**
	 * Renvoie la bonne réponse de l'énigme
	 * @return La bonne réponse
	 */
	public int getReponseEnigme() {
		Enigme e = enigmeCourante();
		
		int i = 1;
		for (Proposition p : e.getPropositionsFR()) {
			if (p.isJuste()) return i;
			i++;
		}
		return 0;
	}

	/** Vérifie si la réponse du joueur est correcte
	 * @param numero La réponse du joueur
	 * @return Si la réponse est correcte
	 */
	public boolean verifierReponse(int numero) {
		if (enigmeCourante().verifierReponse(numero)) {
			this.setEnigmeResolue();
			return true;
		}
		return false;
	}
	
	/** Renvoie la liste des personnages
	 * @param listeEnigmes La liste de toutes les énigmes
	 * @return La liste des personnages
	 * @throws Exception
	 */
	public static ArrayList<PersonnageZone> creerPersonnageZone(ArrayList<Enigme> listeEnigmes) throws Exception {
		ArrayList<String> nomPersonnages = new ArrayList<String>();
		nomPersonnages.add("Père");
		nomPersonnages.add("Mère");
		nomPersonnages.add("Grand-père");
		nomPersonnages.add("Grand-mère");
		
		if (nomPersonnages.size() > listeEnigmes.size() / 2) throw new Exception("Le nombre d'énigmes ne peut pas être inférieur au nombre de personnage");
		
		ArrayList<PersonnageZone> p = new ArrayList<PersonnageZone>();
		int etageEnigme = listeEnigmes.size() / 2;
		for (int i = 0; i < nomPersonnages.size(); i++) {
			ArrayList<Enigme> liste = new ArrayList<Enigme>();
			liste.add(listeEnigmes.get(i));
			liste.add(listeEnigmes.get(i + etageEnigme));
			p.add(new PersonnageZone(nomPersonnages.get(i), liste));
		}
		return p;
	}
	
}

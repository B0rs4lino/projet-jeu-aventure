package carte;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jeu.Affichage;
import jeu.Affichage.LANGUE;

/**
 * @author thomas
 *
 */
/**
 * @author thomas
 *
 */
public class Enigme {

	/**
	 * Question en français 
	 */
	private String questionFR;
	/**
	 * Question en anglais
	 */
	private String questionEN;
	/**
	 * Liste des propositions en français 
	 */
	private ArrayList<Proposition> propositionsFR;
	/**
	 * Liste des propositions en anglais
	 */
	private ArrayList<Proposition> propositionsEN;
	
	/** Constructeur Enigme
	 * @param questionFR La question
	 * @param propositionsFR La liste des propositions
	 */
	public Enigme(String questionFR, ArrayList<Proposition> propositionsFR) {
		super();
		this.questionFR = questionFR;
		this.propositionsFR = propositionsFR;
	}
	
	/** Attribue la question en anglais à l'énigme
	 * @param q La question en anglais
	 * @param p La liste des réponses en anglais
	 */
	public void setEnglish(String q, ArrayList<Proposition> p) {
		this.questionEN = q;
		this.propositionsEN = p;
	}
	
	/** Renvoie la question en anglais
	 * @return La question en anglais
	 */
	public String getQuestionEN() {
		return questionEN;
	}

	/** Renvoie les propositions en anglais
	 * @return La liste des propositions en anglais
	 */
	public ArrayList<Proposition> getPropositionsEN() {
		return propositionsEN;
	}

	/** Renvoie la question selon la langue choisie
	 * @param la La langue choisie
	 * @return La question avec la langue choisie
	 */
	public String getQuestion(LANGUE la) {
		return la == LANGUE.FRANCAIS ? questionFR : questionEN;
	}
	
	/** Vérifie si la réponse entrée est correcte
	 * @param proposition La réponse du joueur
	 * @return Si la réponse est correcte
	 */
	public boolean verifierReponse(int proposition) {
		return this.propositionsFR.get(proposition - 1).isJuste();
	}

	/** Renvoie la question et ses réponses dans la langue choisie
	 * @param la La langue choisie
	 * @return La question et ses réponses dans la langue choisie
	 */
	public String afficher(LANGUE la) {
		String r = getQuestion(la) + "\n";
		int num = 1;
		for (Proposition p : (la == LANGUE.FRANCAIS ? propositionsFR : propositionsEN)) {
			r += num + ": " + p.toString() + "\n";
			num++;
		}
		return r;
	}	
	
	/** Importe les énigmes du fichier JSON
	 * @param path Le chemin vers le fichier JSON
	 * @return La liste des énigmes
	 * @throws IOException
	 * @throws JSONException
	 */
	private static ArrayList<Enigme> importerEnigmesPath(String path) throws IOException, JSONException {
		ArrayList<Enigme> listeEnigme = new ArrayList<Enigme>();
		
		String contenuFichier = new String(Files.readAllBytes(Paths.get(path)));
		JSONArray obj = new JSONArray(contenuFichier);
		
		for (int i = 0; i < obj.length(); i++) {
			
			JSONObject j = obj.getJSONObject(i);
			String question = j.getString("question");
			String correct = j.getString("correcte");
			
			JSONArray incorrecteListe = j.getJSONArray("incorrecte");
			ArrayList<Proposition> listeProposition = new ArrayList<>();
			listeProposition.add(new Proposition(correct, true));
			
			for (int k = 0; k < incorrecteListe.length(); k++) {
				listeProposition.add(new Proposition(incorrecteListe.getString(k)));
			}
			
			Collections.shuffle(listeProposition);
			listeEnigme.add(new Enigme(question, listeProposition));
			
		}
		
		
		return listeEnigme;
	}
	
	/** Importe les énigmes du fichier JSON
	 * @return La liste des énigmes
	 * @throws IOException
	 * @throws JSONException
	 */
	public static ArrayList<Enigme> importerEnigmes() throws IOException, JSONException {
		ArrayList<Enigme> listeEnigmeFR = importerEnigmesPath(Affichage.PATH_ENIGME_FR);
		ArrayList<Enigme> listeEnigmeEN = importerEnigmesPath(Affichage.PATH_ENIGME_EN);
		
		for (int i = 0; i < listeEnigmeFR.size(); i++) {
			Enigme e = listeEnigmeEN.get(i);
			
			// FR car c'est l'inialisation
			listeEnigmeFR.get(i).setEnglish(e.questionFR, e.propositionsFR);
		}
		Collections.shuffle(listeEnigmeFR);
		return listeEnigmeFR;
	}

	/** Renvoie la question en français
	 * @return La question en français
	 */
	public String getQuestionFR() {
		return questionFR;
	}

	/** Renvoie la liste des propositions
	 * @return La liste des propositions
	 */
	public ArrayList<Proposition> getPropositionsFR() {
		return propositionsFR;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof Enigme)) return false;
		Enigme e = (Enigme) obj;
		return this.questionFR == e.questionFR && e.propositionsFR.equals(this.propositionsFR);
	}
	
}

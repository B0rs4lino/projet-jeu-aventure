package carte;

import java.util.ArrayList;
import java.util.Random;

public class Coffre {
	
	/**
	 * Numéro contenu dans le coffre
	 */
	private int numero;
	
	
	/**
	 * Statut du coffre 
	 */
	private boolean deverrouiller;
	
	/** Constructeur du coffre
	 * @param numero Numéro à l'intérieur
	 */
	public Coffre(int numero) {
		super();
		this.numero = numero;
		deverrouiller = false;
	}
	
	/** Constructeur du coffre
	 * @param numero Numéro à l'intérieur
	 * @param deverrouiller Statut du coffre
	 */
	public Coffre(int numero, boolean deverrouiller) {
		super();
		this.numero = numero;
		this.deverrouiller = deverrouiller;
	}
	
	/** Obtenir le numéro à l'intérieur
	 * @return Le numéro du coffre
	 */
	public int getNumero() {
		return this.numero;
	}
	
	/** 
	 * Déverrouille le coffre
	 */
	public void deverrouilleCoffre() {
		deverrouiller = true;
	}

	/** Renvoie le statut du coffre
	 * @return Le statut du coffre
	 */
	public boolean isDeverrouiller() {
		return deverrouiller;
	}
	
	/** Crée tous les coffres avec la longueur du code
	 * @param codeLength Longueur du code
	 * @return La liste des coffres
	 */
	public static ArrayList<Coffre> creerTousCoffres(int codeLength) {
		ArrayList<Coffre> liste = new ArrayList<Coffre>();
		Random r = new Random();
		
		for (int i = 0; i < codeLength; i++) {
			liste.add(new Coffre(r.nextInt(9)));
		}
		return liste;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof Coffre)) return false;
		Coffre c = (Coffre) obj;
		return c.deverrouiller == this.deverrouiller && this.numero == c.numero;
	}
}

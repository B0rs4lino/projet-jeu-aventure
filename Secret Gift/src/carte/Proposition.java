package carte;

public class Proposition {
	/**
	 * Texte de la proposition
	 */
	private String proposition;
	/**
	 * Statut de la proposition
	 */
	private boolean juste;
	
	/** Renvoie le texte de la proposition
	 * @return Le texte de la proposition
	 */
	public String getProposition() {
		return proposition;
	}
	/** Vérifie si la réponse est correcte
	 * @return Si la réponse est juste
	 */
	public boolean isJuste() {
		return juste;
	}
	/** Constructeur proposition
	 * @param proposition Le texte de la proposition
	 * @param juste Si la proposition est correcte
	 */
	public Proposition(String proposition, boolean juste) {
		super();
		this.proposition = proposition;
		this.juste = juste;
	}
	
	/** Constructeur proposition
	 * @param p Le texte de la proposition
	 */
	public Proposition(String p) {
		this(p, false);
	}
	
	@Override
	public String toString() {
		return proposition;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof Proposition)) return false;
		Proposition p = (Proposition) obj;
		return this.juste == p.juste && this.proposition == p.proposition;
	}	
	
}

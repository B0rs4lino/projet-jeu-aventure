package carte;


import jeu.Affichage.LANGUE;
import jeu.Zone;

public class ZoneCoffre extends Zone {
	
	/**
	 * Personnage situé dans la zone
	 */
	private PersonnageZone personnage;
	/**
	 * Coffre situé dans la zone
	 */
	private Coffre coffre;
	/**
	 * Image lorsque le coffre est déverrouillé
	 */
	private String imageCoffreDeverrouille;

	/** Renvoie le coffre de la zone
	 * @return Le coffre de la zone
	 */
	public Coffre getCoffre() {
		return coffre;
	}

	/** Le constructeur de la ZoneCoffre
	 * @param description La description de la zone
	 * @param image Le chemin vers l'image du coffre verrouillé
	 * @param imageDev Le chemin vers l'image du coffre déverrouillé
	 * @param coffre Le coffre correspondant à la zone
	 * @param personnage Le personnage correspondant à la zone
	 */
	public ZoneCoffre(String description, String image, String imageDev, Coffre coffre, PersonnageZone personnage) {
		super(description, image);
		this.coffre = coffre;
		this.personnage = personnage;
		this.imageCoffreDeverrouille = imageDev;
		
		if (this.coffre.isDeverrouiller()) this.setImage(this.imageCoffreDeverrouille);
	}
	
	/** Le constructeur de la ZoneCoffre
	 * @param zone La Zone qui est surchargée par ZoneCoffre
	 * @param imageDev Le chemin vers l'image du coffre déverrouillé
	 * @param coffre Le coffre correspondant à la zone
	 * @param personnage Le personnage correspondant à la zone
	 * @param l La langue choisie par le joueur
	 */
	public ZoneCoffre(Zone zone, String imageDev, Coffre coffre, PersonnageZone personnage, LANGUE l) {
		this(zone.getDescription(l), zone.nomImage(), imageDev, coffre, personnage);
	}
	
	/** Vérifie si l'énigme est résolue pour cette zone
	 * @return Si l'énigme est résolue
	 */
	public boolean verifieEnigmeResolue() {
		return coffre.isDeverrouiller();
	}
	
	/** 
	 * Considérer l'énigme de la zone comme étant résolue
	 */
	public void setEnigmeResolue() {
		coffre.deverrouilleCoffre();
		this.setImage(imageCoffreDeverrouille);
	}
	
	/** Renvoie l'énigme de la zone selon une langue choisie
	 * @param l La langue choisie
	 * @return L'énigme de la zone
	 */
	public String afficherEnigme(LANGUE l) {
		return personnage.afficherEnigme(l);
	}
	
	/** Renvoie si la réponse entrée est correcte pour l'énigme
	 * @param numero Le numéro de la proposition du joueur
	 * @return Si la réponse du joueur est correcte
	 */
	public boolean verifierReponseEnigme(int numero) {
		return personnage.verifierReponse(numero);
	}
	
	/** Renvoie le numéro du coffre
	 * @return Le numéro du coffre
	 */
	public int getCodeCoffre() {
		return coffre.getNumero();
	}
	
	/*
	 * Renvoie la réponse de l'énigme
	 * @return la bonne réponse
	 */
	
	public int getReponseEnigme() {
		return this.personnage.getReponseEnigme();
	}
	
}

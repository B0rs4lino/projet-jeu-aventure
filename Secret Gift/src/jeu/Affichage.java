package jeu;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Affichage {
	/**
	 * Chemin du texte d'affichage en français
	 */
	public static final String PATH_AFFICHAGE_FILE_FR = "fr/affichage.json";
	/**
	 * Chemin des zones en français
	 */
	public static final String PATH_AFFICHAGE_ZONES_FR = "fr/zones.json";
	/**
	 * Chemin des énigmes en français
	 */
	public static final String PATH_ENIGME_FR = "fr/enigmes.json";
	/**
	 * Chemin du texte d'affichage en anglais
	 */
	public static final String PATH_AFFICHAGE_FILE_EN = "en/affichage.json";
	/**
	 * Chemin des zones en anglais
	 */
	public static final String PATH_AFFICHAGE_ZONES_EN = "en/zones.json";
	/**
	 * Chemin des énigmes en anglais
	 */
	public static final String PATH_ENIGME_EN = "en/enigmes.json";
	/**
	 * La langue du jeu choisie par le joueur
	 */
	private LANGUE langueJeu;
	/**
	 * Objet contenant la liste du texte 
	 */
	private JSONObject dialogues;
	
	public enum LANGUE {
		FRANCAIS, ENGLISH;
	}
	
	/** Règle la langue du jeu en fonction de la langue choisie
	 * @param l La langue choisie
	 */
	public void setLangueJeu(LANGUE l) {
		langueJeu = l;
		String contenuFichier;
		try {
			contenuFichier = new String(Files.readAllBytes(Paths.get(l == LANGUE.FRANCAIS ? PATH_AFFICHAGE_FILE_FR : PATH_AFFICHAGE_FILE_EN)));
			dialogues = new JSONObject(contenuFichier);
		} catch (Exception e) {
			dialogues = null;
		}
	}
	
	/** Renvoie la langue choisie par le joueur
	 * @return La langue choisie
	 */
	public LANGUE getLangueJeu() {
		return langueJeu;
	}
	
	/** Constructeur Affichage
	 * @throws JSONException
	 * @throws IOException
	 */
	public Affichage() throws JSONException, IOException {
		langueJeu = LANGUE.FRANCAIS;
		String contenuFichier = new String(Files.readAllBytes(Paths.get(PATH_AFFICHAGE_FILE_FR)));
		dialogues = new JSONObject(contenuFichier);
	}
	
	/** Trouver le nom d'une zone dans une langue voulue sans instance de Affichage
	 * @param nomZone Le nom de la zone recherchée
	 * @param langueJeu La langue voulue
	 * @return
	 */
	public static String trouverNomsZones(String nomZone, LANGUE langueJeu) {
		try {
			String contenuFichier;
			try {
				contenuFichier = new String(Files.readAllBytes(Paths.get(langueJeu == LANGUE.FRANCAIS ? PATH_AFFICHAGE_ZONES_FR : PATH_AFFICHAGE_ZONES_EN)));
				JSONObject zonesNoms = new JSONObject(contenuFichier);
				return zonesNoms.getString(nomZone);
			} catch (IOException e) { return "Fichier manquant"; }
		} catch (JSONException j) {
			j.printStackTrace();
			return "Nom zone manquant !";
		}
	}
	
	/** Trouver un texte dans le fichier JSON dans la langue du joueur
	 * @param nomTexte Le nom du texte à rechercher
	 * @return Le texte dans la langue désirée
	 */
	public String trouverTexte(String nomTexte) {
		try {
			return dialogues.getString(nomTexte) + "\n";
		} catch (JSONException j) {
			j.printStackTrace();
			return "Ligne de dialogue manquante !\n";
		}
	}
	
	/** Trouver un texte dans le fichier JSON dans la langue du joueur sans instance de Affichage
	 * @param nomTexte Le nom du texte à rechercher
	 * @param la La langue désirée
	 * @return Le texte dans la langue désirée
	 * @throws Exception
	 */
	public static String trouverTexteStatic(String nomTexte, LANGUE la) throws Exception {
		String contenuFichier = new String(Files.readAllBytes(Paths.get(la == LANGUE.FRANCAIS ? PATH_AFFICHAGE_FILE_FR : PATH_AFFICHAGE_FILE_EN)));
		JSONObject dialogues = new JSONObject(contenuFichier);
		try {
			return dialogues.getString(nomTexte) + "\n";
		} catch (JSONException j) {
			j.printStackTrace();
			return "Ligne de dialogue manquante !\n";
		}
	}
	
	/** Trouver un texte dans le fichier JSON dans la langue du joueur sans instance de Affichage et sans saut de ligne
	 * @param nomTexte Le nom du texte à rechercher
	 * @param la La langue désirée
	 * @return Le texte dans la langue désirée sans saut de ligne
	 * @throws Exception
	 */
	public static String trouverTexteStaticSansSaut(String nomTexte, LANGUE la) throws Exception {
		return trouverTexteStatic(nomTexte, la).replace("\n", "");
	}
}

package jeu;
import java.util.HashMap;

import jeu.Affichage.LANGUE;

public class Zone 
{
	/**
	 * Description de la zone
	 */
	private String description;
	/**
	 * Chemin vers l'image de la zone
	 */
	private String nomImage;
	/**
	 * HashMap contenant les sorties pour une zone
	 */
	private HashMap<String, Zone> sorties;

	/** Constructeur d'une zone
	 * @param description La description de la zone
	 * @param image Le chemin vers l'image de la zone
	 */
	public Zone(String description, String image) {
		this.description = description;
		nomImage = image;
		sorties = new HashMap<>();
	}
	
	/** Constructeur d'une zone
	 * @param d La description de la zone
	 * @param i Le chemin vers l'image de la zone
	 * @param b 
	 */
	public Zone(String d, String i, boolean b) {
		this(d, i);
	}

	/** Renvoie la description de la zone dans la langue choisie
	 * @param l La langue choisie
	 * @return La description de la zone dans la langue choisie
	 */
	public String getDescription(LANGUE l) {
		return Affichage.trouverNomsZones(description, l);
	}

	/** Ajoute une sorte dans une direction vers la zone
	 * @param sortie La direction vers la zone
	 * @param zoneVoisine La zone à ajouter comme sortie
	 */
	public void ajouteSortie(Sortie sortie, Zone zoneVoisine) {
		sorties.put(sortie.name(), zoneVoisine);
		
		// Ajoute le sens inverse à la sortie zoneVoisine
		zoneVoisine.sorties.put(Sortie.renvoieInverse(sortie).name(), this);
	}
	
	/** Règle la nouvelle image de la zone
	 * @param s Le chemin vers la nouvelle image
	 */
	public void setImage(String s) {
		this.nomImage = s;
	}

	/** Renvoie le chemin de l'image de la zone
	 * @return Le chemin de l'image de la zone
	 */
	public String nomImage() {
		return nomImage;
	}
     
	public String toString() {
		return description;
	}

	/** Renvoie la description longue dans la langue choisie de la zone
	 * @param l La langue choisie 
	 * @return La description longue dans la langue choisie de la zone
	 * @throws Exception
	 */
	public String descriptionLongue(LANGUE l) throws Exception {
		return Affichage.trouverTexteStatic("description_zone", l).replaceFirst("AAAAA", getDescription(l)) + sorties();
	}

	/** Renvoie la liste des sorties disponibles
	 * @return La liste des sorties disponibles
	 */
	private String sorties() {
		return sorties.keySet().toString();
	}

	/** Renvoie la zone voisine d'une direction
	 * @param direction La direction vers la zone voisine
	 * @return Renvoie la zone voisine
	 */
	public Zone obtientSortie(String direction) {
		return sorties.get(direction);
	}
	
}


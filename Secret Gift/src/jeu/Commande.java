package jeu;
import java.util.ArrayList;
import java.util.List;

import jeu.Affichage.LANGUE;

public enum Commande {
	/**
	 * Direction Nord
	 */
	NORD("N"), 
	/**
	 * Direction Sud
	 */
	SUD("S"), 
	/**
	 * Direction Est
	 */
	EST("E"), 
	/**
	 * Direction Ouest
	 */
	OUEST("O"), 
	/**
	 * Direction vers l'étage à partir de la zone de départ
	 */
	ESCALIER("ESC"),
	/**
	 * Utilisation de la téléportation
	 */
	TELEPORTATION("TP"),
	/**
	 * Affiche le menu d'aide contenant les commandes pouvant être utilisées
	 */
	AIDE("?"),
	/**
	 * Liste l'inventaire
	 */
	INVENTAIRE("I"),
	/**
	 * Affiche l'énigme si la zone en contient une
	 */
	ENIGME("EN"),
	/**
	 * Affiche les morceaux de code trouvés par le joueur
	 */
	CODE("C"),
	/**
	 * Affiche le temps restant avant la fin de la partie
	 */
	TIMER("T"),
	/**
	 * Ouvre le cadeau final si le code a été trouvé entièrement
	 */
	CADEAU("CA"),
	/*
	 * Information sur le joker
	 */
	JOKER("J"),
	/**
	 * Quitte la partie
	 */
	QUITTER("Q");

	/** Renvoie l'abréviation d'une commande
	 * @return L'abréviation d'une commande
	 */
	public String getAbreviation() {
		return abreviation;
	}

	/**
	 * Abréviation de la commande
	 */
	private String abreviation;
	/**
	 * Description de la commande
	 */
	private String description;
	/** Le Constructeur d'une commande
	 * @param c L'abréviation de la commande
	 */
	private Commande(String c) { 
		abreviation = c;
		try {
			description = Affichage.trouverTexteStatic("commande_" + this.name().toLowerCase(), LANGUE.FRANCAIS);
		} catch (Exception e) {
			description = "Problème description";
		} 
	}
	@Override
	public String toString() { 
		return name();
	}
	
	/** Trouver une commande avec son abréviation
	 * @param abreviation L'abréviation de la commande recherchée
	 * @return Renvoie la commande trouvée
	 */
	public static Commande findCommande(String abreviation) {
		for (Commande c : values()) {
			if (c.abreviation.equals(abreviation)) return c;
		}
		return null;
	}
	
	/** Renvoie la liste de toutes les descriptions des commandes
	 * @return La liste de toutes les descriptions des commandes
	 */
	public static List<String> toutesLesDescriptions() { 
		ArrayList<String> resultat = new ArrayList<String>();
		for (Commande c : values()) {
			resultat.add( c.description);
		}
		return resultat;
	}
	
	/** Renvoie la liste des descriptions selon une langue choisie
	 * @param l La langue choisie
	 * @return La liste des descriptions selon une langue choisie
	 */
	public static List<String> toutesLesDescriptions(LANGUE l) { 
		ArrayList<String> resultat = new ArrayList<String>();
		for (Commande c : values()) {
			try {
				resultat.add(Affichage.trouverTexteStatic("commande_" + c.name().toLowerCase(), l));
			} catch (Exception e) {
				resultat.add(c.description);
			} 
		}
		return resultat;
	}
	
	/** Renvoie la liste de toutes les abréviations
	 * @return La liste de toutes les abréviations
	 */
	public static List<String> toutesLesAbreviations() { 
		ArrayList<String> resultat = new ArrayList<String>();
		for (Commande c : values()) {
			resultat.add( c.abreviation);
		}
		return resultat;
	}
	
	/** Renvoie la liste des noms de toutes les commandes 
	 * @return La liste des noms de toutes les commandes
	 */
	public static List<String> tousLesNoms() { 
		ArrayList<String> resultat = new ArrayList<String>();
		for (Commande c : values()) {
			resultat.add( c.name());
		}
		return resultat;
	}

}

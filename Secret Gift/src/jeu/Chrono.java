package jeu;

import java.util.Timer;
import java.util.TimerTask;

public class Chrono {
	/**
	 * Objet Timer du Chrono
	 */
	private Timer timer;
	/**
	 * Le temps restant de la partie en seconde
	 */
	private int tempsRestant;
	/**
	 * Le temps par défaut d'une partie
	 */
	public final static int TEMPS_MAX_SEC = 600;
	
	/** Constructeur de Chrono
	 * @param temps Le temps en seconde avant la fin du temps imparti
	 */
	public Chrono(int temps) {
		this.timer = new Timer();
		this.tempsRestant = temps;
	}
	
	/**
	 * Constructeur par défaut à TEMPS_MAX_SEC 
	 */
	public Chrono() {
		this(TEMPS_MAX_SEC);
	}
	
	/** Renvoie le timer de Chrono
	 * @return Le timer de Chrono
	 */
	public Timer getTimer() {
		return timer;
	}
	
	/** Renvoie le temps restant
	 * @return Le temps restant en seconde
	 */
	public int getTempsRestant() {
		return this.tempsRestant;
	}
	
	/**
	 * Retire une seconde au temps restant 
	 */
	public void reduireTemps() {
		this.tempsRestant--;
	}
}

package jeu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import carte.Coffre;
import carte.Enigme;
import carte.PersonnageZone;
import carte.Proposition;
import equipement.CodeFinal;
import equipement.Consommable;
import equipement.Inventaire;
import equipement.Item;
import equipement.NumberConsommableException;
import jeu.Affichage.LANGUE;

public class Sauvegarde {
	/**
	 * Chemin de la sauvegarde par défaut
	 */
	public static final String PATH_DEFAULT_SAUVEGARDE = "sauvegarde.json";
	/**
	 * Dossier contenant la liste des sauvegardes
	 */
	private static final String DIR_PATH_SAUVEGARDE = "sauvegarde/";
	
	/**
	 * Liste des coffres extraits de la sauvegarde 
	 */
	private ArrayList<Coffre> listeCoffres;
	/**
	 * Liste des personnages extraits de la sauvegarde
	 */
	private ArrayList<PersonnageZone> listePersonnage;
	/**
	 * Liste des items extraits de la sauvegarde
	 */
	private ArrayList<Item> listeInventaire;
	/**
	 * Le code secret contenu dans la sauvegarde
	 */
	private String codeSecret;
	/**
	 * Le chemin de la sauvegarde actuelle
	 */
	private String path;
	/**
	 * Temps restant pour la sauvegarde actuelle
	 */
	private int chrono;
	
	/** Renvoie la liste des items de la sauvegarde
	 * @return La liste des items de la sauvegarde
	 */
	public ArrayList<Item> getListeInventaire() {
		return listeInventaire;
	}

	/** Renvoie la liste des coffres de la sauvegarde
	 * @return La liste des coffres de la sauvegarde
	 */
	public ArrayList<Coffre> getListeCoffres() {
		return listeCoffres;
	}
	
	/** Renvoie la liste des personnages de la sauvegarde
	 * @return La liste des personnages de la sauvegarde
	 */
	public ArrayList<PersonnageZone> getListePersonnage() {
		return listePersonnage;
	}
	
	/** Renvoie le temps restant de la sauvegarde
	 * @return Le temps restant de la sauvegarde
	 */
	public int getChrono() {
		return chrono;
	}

	/** Constructeur de Sauvegarde
	 * @param path Le chemin vers la sauvegarde
	 * @throws Exception
	 */
	public Sauvegarde(String path) throws Exception {
		this.path = DIR_PATH_SAUVEGARDE + path;
		
		if (!this.path.endsWith(".json")) this.path += ".json";
		
		File fichier = new File(this.path);
		
		if (!fichier.exists()) {
			this.codeSecret = "";
			this.listeCoffres = Coffre.creerTousCoffres(CodeFinal.LENGTH_CODE);
			this.listeInventaire = new ArrayList<Item>();
			this.listeInventaire.add(new Item(1, "Code final", Commande.CODE));
			this.listeInventaire.add(new Consommable(2, "Joker", Commande.JOKER, 1, 1));
			this.chrono = Chrono.TEMPS_MAX_SEC;
			
			this.listePersonnage = PersonnageZone.creerPersonnageZone(Enigme.importerEnigmes());
		} else {
			this.importerSauvegarde();
		}
	}
	
	/** Constructeur de Sauvegarde avec le chemin par défaut
	 * @throws Exception
	 */
	public Sauvegarde() throws Exception {
		this(Sauvegarde.PATH_DEFAULT_SAUVEGARDE);
	}
	
	/**
	 * Supprime la sauvegarde courante (Partie gagnée ou perdue)
	 */
	public void supprimerSauvegarde() {
		File fichier = new File(this.path);
		fichier.delete();
	}
	
	/** Importer les objets d'une sauvegarde
	 * @throws IOException
	 * @throws JSONException
	 * @throws NumberFormatException
	 */
	public void importerSauvegarde() throws IOException, JSONException, NumberFormatException {
		String contenuFichier = new String(Files.readAllBytes(Paths.get(path)));
		JSONObject obj = new JSONObject(contenuFichier);
		this.codeSecret = (String) obj.get("codeSecret");
		this.listeCoffres = this.importerCoffres(obj.getJSONArray("listeCoffres"));
		this.listeInventaire = this.importerInventaire(obj.getJSONArray("inventaire"));
		this.listePersonnage = this.importerPersonnages(obj.getJSONArray("personnages"));
		this.chrono = obj.getInt("chrono");
		
	}
	
	/** Importer les personnages d'un JSONArray
	 * @param jsonArray JSONArray contenant la liste des personnages de la sauvegarde
	 * @return La liste des personnages de la sauvegarde
	 * @throws JSONException
	 */
	private ArrayList<PersonnageZone> importerPersonnages(JSONArray jsonArray) throws JSONException {
		ArrayList<PersonnageZone> listePersonnage = new ArrayList<>();
		
		// Boucle personnages
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject current = jsonArray.getJSONObject(i);
			JSONArray listesEnigmes = current.getJSONArray("listeEnigmes");
			
			ArrayList<Enigme> listEnigmes = new ArrayList<Enigme>();
			
			// Boucle enigmes
			for (int j = 0; j < listesEnigmes.length(); j++) {
				JSONObject enigme = listesEnigmes.getJSONObject(j);
				JSONArray propositionFR = enigme.getJSONArray("propositionsFR");
				String questionFR = enigme.getString("questionFR");
				
				ArrayList<Proposition> listePropositionsFR = new ArrayList<>();
				//Boucle proposition FR
				for (int k = 0; k < propositionFR.length(); k++) {
					JSONObject proposition = propositionFR.getJSONObject(k);
					listePropositionsFR.add(new Proposition(proposition.getString("proposition"), proposition.getBoolean("juste")));
				}
				
				Enigme e = new Enigme(questionFR, listePropositionsFR);
				
				JSONArray propositionEN = enigme.getJSONArray("propositionsEN");
				String questionEN = enigme.getString("questionEN");
				ArrayList<Proposition> listePropositionsEN = new ArrayList<>();
				//Boucle proposition EN
				for (int k = 0; k < propositionEN.length(); k++) {
					JSONObject proposition = propositionEN.getJSONObject(k);
					listePropositionsEN.add(new Proposition(proposition.getString("proposition"), proposition.getBoolean("juste")));
				}
				
				e.setEnglish(questionEN, listePropositionsEN);
				
				listEnigmes.add(e);
			}
			
			listePersonnage.add(new PersonnageZone(current.getString("nom"), listEnigmes, current.getBoolean("etage")));
		}
		
		return listePersonnage;
	}

	/** Importer l'inventaire d'un JSONArray
	 * @param array Le JSONArray contenant la sauvegarde
	 * @return La liste des items de la sauvegarde
	 * @throws NumberFormatException
	 * @throws JSONException
	 */
	private ArrayList<Item> importerInventaire(JSONArray array) throws NumberFormatException, JSONException {
		ArrayList<Item> liste = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			JSONObject object = array.getJSONObject(i);
			JSONObject o = (JSONObject) object.get("appel");
			
			try {
				Object disponible = object.get("disponible");
				Object maxCapacite = object.get("maxCapacite");
				
				try {
					liste.add(new Consommable((int) object.get("identifiant"), object.getString("nom"), Commande.findCommande(o.getString("abreviation")), (int) disponible, (int) maxCapacite));
				} catch (NumberConsommableException e) {
					liste.add(new Consommable((int) object.get("identifiant"), object.getString("nom"), Commande.findCommande(o.getString("abreviation"))));
				}
					
			} catch (JSONException j) {
				
				// Si l'objet n'est pas un consommable
				liste.add(new Item((int) object.get("identifiant"), object.getString("nom"), Commande.findCommande(o.getString("abreviation"))));
			}
		}
		return liste;
	}

	/** Importer la liste des coffres d'une sauvegarde
	 * @param array JSONArray contenant la liste des coffres
	 * @return La liste des coffres de la sauvegarde
	 * @throws NumberFormatException
	 * @throws JSONException
	 */
	private ArrayList<Coffre> importerCoffres(JSONArray array) throws NumberFormatException, JSONException {
		ArrayList<Coffre> liste = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			JSONObject object = array.getJSONObject(i);
			liste.add(new Coffre((int) object.get("numero"), (Boolean) object.get("deverrouiller")));
		}
		return liste;
	}
	
	/** Renvoie le code trouvé dans la sauvegarde
	 * @return Le code trouvé
	 */
	public String getCode() {
		return codeSecret;
	}
	
	
	/** Crée une sauvegarde lorsqu'un nouveau coffre est ouvert et l'écrit dans le fichier sauvegarde
	 * @param coffre Le coffre à ajouter à la sauvegarde
	 * @param c Le code trouvé à ajouter 
	 * @param inventaire L'inventaire du joueur à sauvegarder
	 * @param tempsRestant Le temps restant à sauvegarder
	 * @throws IOException
	 * @throws JSONException
	 */
	public void sauvegarderNouveauCoffre(Coffre coffre, CodeFinal c, ArrayList<? extends Item> inventaire, int tempsRestant) throws IOException, JSONException {
		File fichier = new File(path);
		
		listeCoffres.set(listeCoffres.indexOf(coffre), coffre);
		codeSecret = c.getCode();
		
		JSONObject obj = new JSONObject();
		obj.put("codeSecret", codeSecret);
		obj.put("listeCoffres", listeCoffres);
		obj.put("inventaire", inventaire);
		obj.put("personnages", listePersonnage);
		obj.put("chrono", tempsRestant);
		
		FileWriter f = new FileWriter(fichier);
		f.write(obj.toString());
		f.close();
	}
	
	/** Trouver les sauvegardes existantes
	 * @param la Afficher le menu dans la langue choisie
	 * @return Affichage de la liste des sauvegardes
	 */
	public static ArrayList<String> listerSauvegardes(LANGUE la) {
		File dir = new File(DIR_PATH_SAUVEGARDE);
		if (!dir.exists()) dir.mkdir();
		
		File[] files = dir.listFiles((d, name) -> name.endsWith(".json")); // && name.startsWith("sauvegarde"));
		
		ArrayList<String> liste = new ArrayList<String>();
		try {
			liste.add(Affichage.trouverTexteStaticSansSaut("creer_nouvelle_sauvegarde", la));
		} catch (Exception e) {
			liste.add("Créer une nouvelle sauvegarde");
		}
		
		for (File f : files) {
			liste.add(f.getName().substring(0, f.getName().length() - 5));
		}
		return liste;
	}
	
	/** Vérifie si la sauvegarde existe pour un nom donné
	 * @param nom Le nom de la sauvegarde
	 * @return Si la sauvegarde existe
	 */
	public static boolean verifierSauvegardeExiste(String nom) {
		File fichier = new File(DIR_PATH_SAUVEGARDE + nom + ".json");
		
		return fichier.exists();
	}
}

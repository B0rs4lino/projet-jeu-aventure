package jeu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;

import carte.Coffre;
import carte.Enigme;
import carte.PersonnageZone;
import carte.ZoneCoffre;
import equipement.CodeFinal;
import equipement.Consommable;
import equipement.Inventaire;
import equipement.NumberConsommableException;
import jeu.Affichage.LANGUE;

public class Jeu {
	
	/**
	 * Interface graphique du jeu
	 */
	private GUI gui; 
	/**
	 * Zone actuelle où se situe le joueur
	 */
	private Zone zoneCourante;
	/**
	 * La zone de départ au début de la partie
	 */
	private Zone zoneDepart;
	/**
	 * Historique des zones parcourues par le joueur
	 */
	private LinkedList<Zone> zonesPrecedentes;
	/**
	 * Statut si une énigme est en cours
	 */
	private boolean enigmeEnCours;
	/**
	 * Code final trouvé de la partie courante
	 */
	private CodeFinal code;
	/**
	 * Nombre de mauvaises réponses commises par le joueur
	 */
	private int nombreMauvaiseReponse;
	/**
	 * Nombre maximum de mauvaises réponses
	 */
	private final static int MAX_MAUVAISES_REPONSES = 1; 
	/**
	 * Sauvegarde actuelle
	 */
	private Sauvegarde sauvegarde;
	/**
	 * Objet contenant les textes traduits
	 */
	private Affichage affichageText;
	/**
	 * Statut du choix de la langue par l'utilisateur
	 */
	private boolean langueChoisie = false;
	/**
	 * Statut du choix de la sauvegarde par l'utilisateur
	 */
	private boolean sauvegardeChoisie = false;
	/**
	 * Statut du choix du nom de la sauvegarde par l'utilisateur
	 */
	private boolean editionNomSauvegarde = false;
	/**
	 * Liste des sauvegardes trouvées dans le dossier des sauvegardes
	 */
	private ArrayList<String> listeSauvegarde;
	/**
	 * Chrono de la partie courante 
	 */
	private Chrono chrono;
	
	/**
	 * Statut pour l'accès à l'étage
	 */
	private boolean accesEtage = false;
	/**
	 * Inventaire du joueur
	 */
	private Inventaire inventaireJoueur;
    
	/** Constructeur de jeu
	 * @throws Exception
	 */
	public Jeu() throws Exception {
		zonesPrecedentes = new LinkedList<>();
		code = new CodeFinal();
		affichageText = new Affichage();
		nombreMauvaiseReponse = 0;
		enigmeEnCours = false;
		inventaireJoueur = new Inventaire();
		gui = null;
	}

	/** Règle l'interface graphique
	 * @param g L'interface graphique
	 * @throws JSONException
	 * @throws IOException
	 */
	public void setGUI( GUI g) throws JSONException, IOException { 
		gui = g;	
		afficherMessageDeBienvenue();
	}
	
	/**
	 * Le choix de la langue
	 */
	private void choixLangue() {
		gui.afficher("Veuillez choisir une langue...\n");
		int i = 0;
		for (LANGUE l : LANGUE.values()) {
			i++;
			gui.afficher(i + " " + l.name() + "\n");
			
		}
	}
    
	/** Création de la carte
	 * @throws Exception
	 */
	private void creerCarte() throws Exception {
		
		ArrayList<Zone> zonesAvecCoffres = new ArrayList<Zone>();
		
		ArrayList<Coffre> listeCoffres = sauvegarde.getListeCoffres();
		ArrayList<PersonnageZone> listePersonnage = sauvegarde.getListePersonnage();
		inventaireJoueur.setListeObjet(sauvegarde.getListeInventaire());
		code = new CodeFinal(sauvegarde.getCode());
		
		chrono = new Chrono(sauvegarde.getChrono());
		demarrerChrono();
		
		
		// Rez-de-chaussée
		zonesAvecCoffres.add(new Zone("hall_entree", "rezChaussee/hall_entree.jpg"));          
		zonesAvecCoffres.add(new Zone("couloir_rez_chaussee", "rezChaussee/couloir.jpg"));
		zonesAvecCoffres.add(new ZoneCoffre("cuisine", "rezChaussee/verrouille/cuisine.jpg", "rezChaussee/deverrouille/cuisine.jpg", listeCoffres.get(0), listePersonnage.get(0)));                    
		zonesAvecCoffres.add(new ZoneCoffre("atelier", "rezChaussee/verrouille/atelier.jpg", "rezChaussee/deverrouille/atelier.jpg", listeCoffres.get(1), listePersonnage.get(1)));                     
		zonesAvecCoffres.add(new ZoneCoffre("salle_manger", "rezChaussee/verrouille/salle_manger.jpg", "rezChaussee/deverrouille/salle_manger.jpg", listeCoffres.get(2), listePersonnage.get(2)));        
		zonesAvecCoffres.add(new ZoneCoffre("salon", "rezChaussee/verrouille/salon.jpg", "rezChaussee/deverrouille/salon.jpg", listeCoffres.get(3), listePersonnage.get(3)));
		
		zonesAvecCoffres.get(0).ajouteSortie(Sortie.OUEST, zonesAvecCoffres.get(2));
		zonesAvecCoffres.get(0).ajouteSortie(Sortie.EST, zonesAvecCoffres.get(4));
		zonesAvecCoffres.get(0).ajouteSortie(Sortie.NORD, zonesAvecCoffres.get(1));
		zonesAvecCoffres.get(1).ajouteSortie(Sortie.OUEST, zonesAvecCoffres.get(3));
		zonesAvecCoffres.get(1).ajouteSortie(Sortie.EST, zonesAvecCoffres.get(5));
		
		
		// Etage
		zonesAvecCoffres.add(new ZoneCoffre("chambre_parents", "etage/verrouille/chambre_gauche.jpg", "etage/deverrouille/chambre_gauche.jpg", listeCoffres.get(4), listePersonnage.get(0)));
		zonesAvecCoffres.add(new ZoneCoffre("chambre_invites", "etage/verrouille/chambre_enhaut_droite.jpg", "etage/deverrouille/chambre_enhaut_droite.jpg", listeCoffres.get(5), listePersonnage.get(1)));
		zonesAvecCoffres.add(new ZoneCoffre("chambre_joelle", "etage/verrouille/chambre_enbas_droite.jpg", "etage/deverrouille/chambre_enbas_droite.jpg", listeCoffres.get(6), listePersonnage.get(2)));
		zonesAvecCoffres.add(new Zone("haut_escalier", "etage/couloir_au_milieu.jpg"));
		zonesAvecCoffres.add(new Zone("couloir_droit_etage", "etage/couloir_droite.jpg"));
		zonesAvecCoffres.add(new Zone("couloir_gauche_etage", "etage/couloir_gauche.jpg"));
		zonesAvecCoffres.add(new ZoneCoffre("salle_etude", "etage/verrouille/salle_etude.jpg", "etage/deverrouille/salle_etude.jpg", listeCoffres.get(7), listePersonnage.get(3)));
		zonesAvecCoffres.get(9).ajouteSortie(Sortie.OUEST, zonesAvecCoffres.get(11));
		zonesAvecCoffres.get(11).ajouteSortie(Sortie.NORD, zonesAvecCoffres.get(12));
		zonesAvecCoffres.get(11).ajouteSortie(Sortie.SUD, zonesAvecCoffres.get(6));
		zonesAvecCoffres.get(9).ajouteSortie(Sortie.EST, zonesAvecCoffres.get(10));
		zonesAvecCoffres.get(10).ajouteSortie(Sortie.SUD, zonesAvecCoffres.get(8));
		zonesAvecCoffres.get(10).ajouteSortie(Sortie.NORD, zonesAvecCoffres.get(7));
		
		// Escalier
		zonesAvecCoffres.get(0).ajouteSortie(Sortie.ESCALIER, zonesAvecCoffres.get(9));
		
		
		zoneCourante = zonesAvecCoffres.get(0);
		zoneDepart = zonesAvecCoffres.get(0);
	
	}

	/**
	 * Afficher la zone courante
	 */
	private void afficherLocalisation() {
		try {
			gui.afficher(zoneCourante.descriptionLongue(affichageText.getLangueJeu()));
		} catch (Exception e) {
			gui.afficher("Problème description\n");
		}
		gui.afficher();
	}
	
	/**
	 * Déverrouille l'accès à l'étage
	 */
	private void deverrouillerEtage() {
		accesEtage = true;
		gui.afficher(affichageText.trouverTexte("etage_deverrouille"));
	}

	/**
	 * Affiche le message de bienvenue et le menu du choix de la langue
	 */
	private void afficherMessageDeBienvenue() {
		gui.afficher(affichageText.trouverTexte("message_bienvenue"));
		choixLangue();
	}
	
	/** Affiche la liste des langues pour que le joueur puisse choisir
	 * @param commandeLue L'entrée de l'utilisateur pour son choix
	 */
	private void affichageLangue(String commandeLue) {
		try {
			int reponse = Integer.parseInt(commandeLue);
			if (reponse - 1 < LANGUE.values().length && reponse - 1 >= 0) {
				affichageText.setLangueJeu(reponse == 1 ? LANGUE.FRANCAIS : LANGUE.ENGLISH);
				langueChoisie = true;
			}
			else choixLangue();
		} catch (NumberFormatException n) {
			gui.afficher("La réponse n'est pas un nombre !\n");
			choixLangue();
		}
	}
	
	/**
	 * Initialisation de la carte
	 */
	private void initialiserCarte() {
		try {
			creerCarte();
			afficherLocalisation();
			gui.afficheImage(zoneCourante.nomImage());
			if (code.codeTrouveMoitie()) deverrouillerEtage();
		}
		catch (Exception e) {
			e.printStackTrace();
			gui.afficher("Erreur carte");
		}
	}
	
	/**
	 * Menu affichage pour le choix de la sauvegarde
	 */
	private void affichageChoixSauvegarde() {
		for (int i = 0; i < listeSauvegarde.size(); i++) {
			gui.afficher(i + ". " + listeSauvegarde.get(i) + "\n");
		}
		gui.afficher();
	}
	
	/** Choix de la sauvegarde par le joueur
	 * @param commandeLue Le choix du joueur
	 */
	private void choixSauvegarde(String commandeLue) {
		try {
			int reponse = Integer.parseInt(commandeLue);
			if (reponse < listeSauvegarde.size() && reponse >= 0) {
				if (reponse == 0) {
					gui.afficher(affichageText.trouverTexte("nom_nouvelle_sauvegarde"));
					editionNomSauvegarde = true;
				}
				else {
					sauvegardeChoisie = true;
					try {
						sauvegarde = new Sauvegarde(listeSauvegarde.get(reponse));
					} catch (Exception e) {
						gui.afficher(affichageText.trouverTexte("probleme_sauvegarde"));
					}
				}
			}
			else {
				gui.afficher(affichageText.trouverTexte("nombre_reponse_incorrecte"));
				affichageChoixSauvegarde();
			}
		} catch (NumberFormatException n) {
			gui.afficher(affichageText.trouverTexte("reponse_pas_nombre"));
			affichageChoixSauvegarde();
		}
	}
    
	/** Traitement de la saisie utilisateur
	 * @param commandeLue La commande entrée par l'utilisateur
	 */
	public void traiterCommande(String commandeLue) {
		gui.afficher( "> " + commandeLue + "\n");
		
		if (!langueChoisie) {
			affichageLangue(commandeLue);
			listeSauvegarde = Sauvegarde.listerSauvegardes(affichageText.getLangueJeu());
			affichageChoixSauvegarde();
		}
		else if (!sauvegardeChoisie) {
			
			if (editionNomSauvegarde) {
				if (Sauvegarde.verifierSauvegardeExiste(commandeLue)) {
					gui.afficher(affichageText.trouverTexte("sauvegarde_existe"));
				} else {
					gui.afficher(affichageText.trouverTexte("sauvegarde_creee"));
					editionNomSauvegarde = false;
					sauvegardeChoisie = true;
					try {
						this.sauvegarde = new Sauvegarde(commandeLue);
					} catch (Exception e) {
						gui.afficher(affichageText.trouverTexte("probleme_sauvegarde"));
					}
				}
			} else {
				choixSauvegarde(commandeLue);
			}
			
			if (sauvegardeChoisie) {
				initialiserCarte();
			}
		}
		else if (enigmeEnCours) {
			try {
				ZoneCoffre z = ((ZoneCoffre) zoneCourante);
				
				
				if (commandeLue.toUpperCase().equals("J") || commandeLue.toUpperCase().equals("JOKER")) {
					joker();
					return;
				}
				
				if (z.verifierReponseEnigme(Integer.parseInt(commandeLue))) {
					gui.afficher(affichageText.trouverTexte("bonne_reponse_enigme"));
					gui.afficher(affichageText.trouverTexte("afficher_morceau_trouve") + z.getCodeCoffre() + "\n");
					z.setEnigmeResolue();
					code.ajouterNumero(z.getCodeCoffre());
					gui.afficheImage(zoneCourante.nomImage());
					
					if (!accesEtage && code.codeTrouveMoitie()) {
						deverrouillerEtage();
						String tpNom = affichageText.trouverTexte("tp_nom");
						try {
							Consommable c = new Consommable(0, tpNom, Commande.TELEPORTATION, 1);
							inventaireJoueur.ajouterItem(c);
							gui.afficher(affichageText.trouverTexte("nouvel_item") + c.toString() + "\n");
						} catch (NumberConsommableException n) {
							gui.afficher(affichageText.trouverTexte("probleme_item") + tpNom + "\n");
						}
					}
					else if (accesEtage && code.codeTrouveEntierement()) codeTrouve();
					
					try {
						this.sauvegarde.sauvegarderNouveauCoffre(z.getCoffre(), code, inventaireJoueur.getListe(), chrono.getTempsRestant());
					} catch (IOException | JSONException e) {
						gui.afficher(affichageText.trouverTexte("echec_sauvegarde"));
					}
				}
				else {
					gui.afficher(affichageText.trouverTexte("mauvaise_reponse"));
					nombreMauvaiseReponse++;
					
					if (nombreMauvaiseReponse > Jeu.MAX_MAUVAISES_REPONSES) {
						gui.afficher(affichageText.trouverTexte("jeu_perdu"));
						sauvegarde.supprimerSauvegarde();
						terminer();
					} else {
						int chances = Jeu.MAX_MAUVAISES_REPONSES - nombreMauvaiseReponse;
						
						if (chances > 0) {
							gui.afficher(affichageText.trouverTexte("chances_restantes").replaceFirst("AAAAA", String.valueOf(chances)));
						} else {
							gui.afficher(affichageText.trouverTexte("derniere_chance"));
						}
					}
				}
			}
			catch (NumberFormatException n) {
				gui.afficher(affichageText.trouverTexte("reponse_pas_nombre"));
			}
			enigmeEnCours = false;
		}
		else {
		
			switch (commandeLue.toUpperCase()) {
				case "?" : case "AIDE" : 
					afficherAide(); 
					break;
				case "N" : case "NORD" :
					allerEn( "NORD"); 
					break;
				case "S" : case "SUD" :
					allerEn( "SUD"); 
					break;
				case "E" : case "EST" :
					allerEn( "EST"); 
					break;
				case "O" : case "OUEST" :
					allerEn( "OUEST"); 
					break;
				case "Q" : case "QUITTER" :
					terminer();
					break;
				case "ESC": case "ESCALIER" :
					if (prendreEscalier()) allerEn("ESCALIER");
					break;
				case "TELEPORTATION": case "TP":
					teleportation();
					break;
				case "INVENTAIRE": case "I":
					afficherInventaire();
					break;
				case "ENIGME": case "EN":
					afficherEnigme();
					break;
				case "JOKER": case "J":
					joker();
					break;
				case "CODE": case "C":
					afficherCode();
					break;
				case "CADEAU": case "CA":
					ouvrirCadeau();
					break;
				case "REVENIR": case "R":
					revenirZone();
					break;
				case "TIMER": case "T":
					timer();
					break;
				case "TEST":
					testGame();
					break;
				default : 
					gui.afficher(affichageText.trouverTexte("commande_inconnue"));
					break;
	        	}
		}
    	}
	
	private void testGameCommand(BufferedReader b) {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			private int compteur = 1;
			
			@Override
			public void run() {
				
				if (compteur <= 0) {
					String command = null;
					try {
						command = b.readLine();
					} catch (IOException e) {
						e.printStackTrace();
					}
					if (command != null) {
						if (command.equals("reponseEnigme")) {
							testEnigme();
						}
						else traiterCommande(command);
						testGameCommand(b);
					}
					timer.cancel();
				}
				compteur--;
				
			}
		}, 0, 1000);
	}
	
	/**
	 * Répond automatiquement à l'énigme
	 */
	
	private void testEnigme() {
		if (enigmeEnCours) {
			int reponse = ((ZoneCoffre) this.zoneCourante).getReponseEnigme();
			traiterCommande(String.valueOf(reponse));
		}
	}
	
	/**
	 * Fonction de test pour terminer le jeu
	 */
	
	private void testGame() {
		File f = new File("./gameTest.txt");
		
		try {
			BufferedReader b = new BufferedReader(new FileReader(f));
			testGameCommand(b);
			
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * Affichage de la commande pour le temps restant
	 */
	private void timer() {
		gui.afficher(affichageText.trouverTexte("temps_restant") + chrono.getTempsRestant() + "s");		
	}
	
	/**
	 * Démarrage du chrono (temps de la partie)
	 */
	private void demarrerChrono() {
		Timer timer = chrono.getTimer();
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				chrono.reduireTemps();
				
				if (chrono.getTempsRestant() < 0) {
					timer.cancel();
					gui.afficher("\n" + affichageText.trouverTexte("temps_perdu"));
					sauvegarde.supprimerSauvegarde();
					terminer();
				}
				
			}
		}, 0, 1000);
	}

	/**
	 * Revenir dans la zone précédente
	 */
	private void revenirZone() {
		if (zonesPrecedentes.size() > 0) {
			Zone zone = zonesPrecedentes.pop();
			gui.afficher(affichageText.trouverTexte("retour_arriere") + zone.getDescription(affichageText.getLangueJeu()));
			zoneCourante = zone;
			gui.afficheImage(zoneCourante.nomImage());
		} else {
			gui.afficher(affichageText.trouverTexte("retour_arriere_impossible"));
		}
	}
	
	/**
	 * Ouvrir le cadeau final à l'entrée du jeu 
	 */
	private void ouvrirCadeau() {
		if (zoneCourante.equals(zoneDepart)) {
			if (code.codeTrouveEntierement()) {
				
				gui.afficher(affichageText.trouverTexte("partie_reussie"));
				gui.afficheImage("rezChaussee/hall_entree_fin.jpg");
				sauvegarde.supprimerSauvegarde();
				terminer();
				
			} else {
				gui.afficher(affichageText.trouverTexte("code_pas_trouve"));
			}
			
		}
		else {
			gui.afficher(affichageText.trouverTexte("coffre_mauvaise_zone"));
		}
	}

	/**
	 * Informe le joueur que le code est complet
	 */
	private void codeTrouve() {
		gui.afficher(affichageText.trouverTexte("code_trouve_entierement"));
		
		if (inventaireJoueur.consommableDisponible(0)) gui.afficher(affichageText.trouverTexte("avertissement_teleportation"));
		
	}

	/**
	 * Affiche le contenu de l'inventaire
	 */
	private void afficherInventaire() {
		gui.afficher(inventaireJoueur.listeInventaire());
	}
	
	/**
	 * Affiche les morceaux de code trouvés par l'utilisateur
	 */
	private void afficherCode() {
		if (code.getCode() == "") {
			gui.afficher(affichageText.trouverTexte("aucun_code_trouve"));
		}
		else {
			String affichage = code.getCode();
			while (affichage.length() <= CodeFinal.LENGTH_CODE) {
				affichage += "-";
			}
			gui.afficher(affichageText.trouverTexte("code_trouve_affichage") + affichage);
		}
	}
	
	/**
	 * Afficher l'énigme de la zone courante
	 */
	private void afficherEnigme() {
		if (!(zoneCourante instanceof ZoneCoffre)) {
			gui.afficher(affichageText.trouverTexte("aucune_enigme_zone"));
		}
		else if (((ZoneCoffre) zoneCourante).verifieEnigmeResolue()) {
			gui.afficher(affichageText.trouverTexte("enigme_deja_resolue"));
		} else {
			
			if (inventaireJoueur.consommableDisponible(2)) {
				gui.afficher(affichageText.trouverTexte("joker_info"));
			}
			
			gui.afficher(((ZoneCoffre) zoneCourante).afficherEnigme(affichageText.getLangueJeu()));
			enigmeEnCours = true;
		}
	}

	/**
	 * Lancer la téléportation détenue par le joueur dans son inventaire
	 */
	private void teleportation() {
		if (inventaireJoueur.utiliserConsommable(0) == 1) {
			this.zoneCourante = zoneDepart;
			gui.afficher(affichageText.trouverTexte("teleportation_activee"));
			gui.afficheImage(zoneCourante.nomImage());
		} else {
			gui.afficher(affichageText.trouverTexte("teleportation_indisponible"));
		}
		gui.afficher();
	}
	
	/*
	 * Utiliser le joker pour passer une question
	 */
	
	private void joker() {
		boolean b = inventaireJoueur.consommableDisponible(2);
		if (enigmeEnCours) {
			if (b) {
				inventaireJoueur.utiliserConsommable(2);
				gui.afficher();
				int reponse = ((ZoneCoffre) this.zoneCourante).getReponseEnigme();
				traiterCommande(String.valueOf(reponse));
			} else {
				gui.afficher(affichageText.trouverTexte("joker_indispo") + affichageText.trouverTexte("joker_retaper_reponse"));
				
			}
		} else {
			gui.afficher(b ? affichageText.trouverTexte("joker_dispo") : affichageText.trouverTexte("joker_indispo"));
		}
	}
	
	/** Accéder à l'étage si celui-ci est débloqué
	 * @return Renvoie l'accessibilité de l'étage
	 */
	private boolean prendreEscalier() {
		if (accesEtage) return true;
		gui.afficher(affichageText.trouverTexte("avertissement_etage_bloque"));
		gui.afficher();
		return false;
	}

	/**
	 * Affichage du menu d'aide pour les commandes
	 */
	private void afficherAide() {
		gui.afficher(affichageText.trouverTexte("menu_aide_affichage"));
		gui.afficher(Commande.toutesLesDescriptions(affichageText.getLangueJeu()).toString());
		gui.afficher();
	}

	/** Méthode de déplacement du joueur
	 * @param direction Direction souhaitée par le joueur
	 */
	private void allerEn(String direction) {
		zonesPrecedentes.push(zoneCourante);
		Zone nouvelle = zoneCourante.obtientSortie( direction);
		if ( nouvelle == null ) {
			gui.afficher(affichageText.trouverTexte("pas_de_sortie") + direction);
			gui.afficher();
		}
		else {
			zoneCourante = nouvelle;
			try {
				gui.afficher(zoneCourante.descriptionLongue(affichageText.getLangueJeu()));
			} catch (Exception e) {
				gui.afficher("Problème description\n");
			}
			gui.afficher();
			gui.afficheImage(zoneCourante.nomImage());
		}
	}
    
	/**
	 * Termine la partie
	 */
	private void terminer() {
		gui.afficher(affichageText.trouverTexte("au_revoir"));
		gui.enable( false);
	}
}

package jeu;
public enum Sortie {
	NORD, SUD, EST, OUEST, ESCALIER;
	
	public static Sortie renvoieInverse(Sortie s) {
		switch (s) {
			case NORD:
				return SUD;
			case SUD:
				return NORD;
			case EST:
				return OUEST;
			case OUEST:
				return EST;
			default:
				return ESCALIER;
		}
	}
}

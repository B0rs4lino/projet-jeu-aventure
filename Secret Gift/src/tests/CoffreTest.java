package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import carte.Coffre;

class CoffreTest {
	
	private Coffre coffre;

	@BeforeEach
	void setUp() throws Exception {
		coffre = new Coffre(1);
	}

	@Test
	void testCoffreInt() {
		Coffre c1 = new Coffre(1);
		assertTrue(c1 instanceof Coffre);
	}

	@Test
	void testCoffreIntBoolean() {
		Coffre c1 = new Coffre(1, false);
		assertFalse(c1.isDeverrouiller());
		assertTrue(c1 instanceof Coffre);
	}

	@Test
	void testGetNumero() {
		assertEquals(coffre.getNumero(), 1);
	}

	@Test
	void testIsDeverrouiller() {
		assertFalse(coffre.isDeverrouiller());
		coffre.deverrouilleCoffre();
		assertTrue(coffre.isDeverrouiller());
	}

	@Test
	void testCreerTousCoffres() {
		ArrayList<Coffre> listeCoffres = Coffre.creerTousCoffres(3);
		assertEquals(listeCoffres.size(), 3);
	}
	
	@Test
	void testEquals() {
		assertFalse(coffre.equals(new Coffre(2)));
		assertEquals(coffre, new Coffre(1));
	}

}

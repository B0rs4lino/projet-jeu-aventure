package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import jeu.Zone;
import jeu.Affichage.LANGUE;
import jeu.Sortie;

class ZoneTest {

	private Zone zone, zone2;
	
	@BeforeEach
	void setUp() throws Exception {
		zone = new Zone("description", "image");
		zone2 = new Zone("descrption2", "image");
	}

	@Test
	void testSetImage() {
		assertEquals(zone.nomImage(), "image");
		zone.setImage("image2");
		assertEquals(zone.nomImage(), "image2");
	}

	@Test
	void testNomImage() {
		assertEquals(zone.nomImage(), "image");
	}

	@Test
	void testToString() {
		assertEquals(zone.toString(), "description");
	}

	@Test
	void testAjouteEtObtientSortie() {
		zone.ajouteSortie(Sortie.NORD, zone2);
		assertEquals(zone.obtientSortie("NORD"), zone2);
	}

}

package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import jeu.Affichage;
import jeu.Affichage.LANGUE;

class AffichageTest {

	private Affichage affichage;
	
	@BeforeEach
	void setUp() throws Exception {
		affichage = new Affichage();
		affichage.setLangueJeu(LANGUE.FRANCAIS);
	}

	@Test
	void testSetLangueJeuAndGetLangueJeu() {
		affichage.setLangueJeu(LANGUE.FRANCAIS);
		assertEquals(affichage.getLangueJeu(), LANGUE.FRANCAIS);
		affichage.setLangueJeu(LANGUE.ENGLISH);
		assertEquals(affichage.getLangueJeu(), LANGUE.ENGLISH);
	}

	@Test
	void testAffichage() {
		assertTrue(affichage instanceof Affichage);
	}

	@Test
	void testTrouverNomsZones() {
		assertEquals(Affichage.trouverNomsZones("atelier", LANGUE.FRANCAIS), "l'Atelier");
		assertEquals(Affichage.trouverNomsZones("atelier", LANGUE.ENGLISH), "the workshop");
	}

	@Test
	void testTrouverTexte() {
		assertEquals(affichage.trouverTexte("etage_deverrouille"), "Etage déverrouillé\n");
		affichage.setLangueJeu(LANGUE.ENGLISH);
		assertEquals(affichage.trouverTexte("etage_deverrouille"), "Floor unlocked\n");
	}

	@Test
	void testTrouverTexteStatic() throws Exception {
		assertEquals(Affichage.trouverTexteStatic("etage_deverrouille", LANGUE.FRANCAIS), "Etage déverrouillé\n");
		assertEquals(Affichage.trouverTexteStatic("etage_deverrouille", LANGUE.ENGLISH), "Floor unlocked\n");
		assertEquals(Affichage.trouverTexteStatic("etage_dever", LANGUE.FRANCAIS), "Ligne de dialogue manquante !\n");
	}

	@Test
	void testTrouverTexteStaticSansSaut() throws Exception {
		assertEquals(Affichage.trouverTexteStaticSansSaut("etage_deverrouille", LANGUE.FRANCAIS), "Etage déverrouillé");
		assertEquals(Affichage.trouverTexteStaticSansSaut("etage_deverrouille", LANGUE.ENGLISH), "Floor unlocked");
		assertEquals(Affichage.trouverTexteStatic("etage_dever", LANGUE.FRANCAIS), "Ligne de dialogue manquante !\n");
	}

}

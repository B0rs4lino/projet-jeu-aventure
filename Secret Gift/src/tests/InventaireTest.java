package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import equipement.Consommable;
import equipement.Inventaire;
import equipement.Item;
import equipement.NumberConsommableException;
import jeu.Commande;

class InventaireTest {
	
	Inventaire inventaire;

	@BeforeEach
	void setUp() throws Exception {
		inventaire = new Inventaire();
	}

	@Test
	void testInventaire() {
		assertTrue(inventaire instanceof Inventaire);
	}

	@Test
	void testAjouterItem() {
		assertEquals(0, inventaire.getListe().size());
		inventaire.ajouterItem(new Item(10, "TestItem", Commande.CODE));
		assertEquals(1, inventaire.getListe().size());
		inventaire.ajouterItem(new Item(11, "TestItem2", Commande.CADEAU));
		assertEquals(2, inventaire.getListe().size());
	}

	@Test
	void testAjouterListeItem() {
		assertEquals(inventaire.getListe().size(), 0);
		ArrayList<Item> listeItem = new ArrayList<>(Arrays.asList(new Item(10, "TestItem", Commande.CODE), new Item(11, "TestItem2", Commande.CADEAU)));
		inventaire.ajouterListeItem(listeItem);
		assertEquals(inventaire.getListe().size(), 2);
	}

	@Test
	void testSetListeObjetAndGetListe() {
		ArrayList<Item> listeItem = new ArrayList<>(Arrays.asList(new Item(10, "TestItem", Commande.CODE), new Item(11, "TestItem2", Commande.CADEAU)));
		inventaire.setListeObjet(listeItem);
		assertEquals(listeItem, inventaire.getListe());
	}

	@Test
	void testListeInventaire() {
		assertEquals(inventaire.listeInventaire(), "Liste des objets de l'inventaire :\n"
				+ "L'inventaire est vide.\n");
		inventaire.ajouterItem(new Item(11, "Test", Commande.CODE));
		inventaire.ajouterItem(new Item(11, "Test2", Commande.TELEPORTATION));
		assertEquals(inventaire.listeInventaire(), "Liste des objets de l'inventaire :\n"
				+ "Test -> C\n" + "Test2 -> TP\n");
	}

	@Test
	void testConsommableDisponibleAndUtiliserConsommable() throws NumberConsommableException {
		Consommable c1 = new Consommable(11, "Test", Commande.CODE, 1, 1);
		inventaire.ajouterItem(c1);
		assertTrue(inventaire.consommableDisponible(11));
		inventaire.utiliserConsommable(11);
		assertFalse(inventaire.consommableDisponible(11));
	}

}

package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import carte.Enigme;
import carte.Proposition;
import jeu.Affichage.LANGUE;

class EnigmeTest {
	
	private Enigme e;

	@BeforeEach
	void setUp() throws Exception {
		ArrayList<Proposition> listePropositions = new ArrayList<>();
		listePropositions.add(new Proposition("Sapin : Bonne réponse", true));
		listePropositions.add(new Proposition("Autre : Mauvaise réponse"));
		listePropositions.add(new Proposition("Autres2: Mauvaise réponse"));
		listePropositions.add(new Proposition("Autres3: Mauvaise réponse"));
		
		e = new Enigme("Mon premier est un pronom possessif, mon second se trouve chez le boulanger, mon tout est un arbre", listePropositions);
	}

	@Test
	void testEnigme() {
		assertTrue(e instanceof Enigme);
	}

	@Test
	void testSetEnglishAndGetQuestion() {
		ArrayList<Proposition> listePropositions = new ArrayList<Proposition>();
		listePropositions.add(new Proposition("Kitten : Good Answer", true));
		listePropositions.add(new Proposition("Other : Bad answer"));
		listePropositions.add(new Proposition("Others2: Bad answer"));
		listePropositions.add(new Proposition("Others3: Bad answer"));
		
		e.setEnglish("My first is the eye of a needle, My second is a fish and My everything is a baby animal", listePropositions);
		assertEquals(e.getQuestion(LANGUE.ENGLISH), "My first is the eye of a needle, My second is a fish and My everything is a baby animal");
	}

	@Test
	void testVerifierReponse() {
		assertFalse(e.verifierReponse(2));
		assertTrue(e.verifierReponse(1));
	}

	@Test
	void testAfficher() {
		String attendu = e.getQuestion(LANGUE.FRANCAIS) + "\n";
		int num = 1;
		for (Proposition p : e.getPropositionsFR()) {
			attendu += num + ": " + p.toString() + "\n";
			num++;
		}
		
		String reponse = "Mon premier est un pronom possessif, mon second se trouve chez le boulanger, mon tout est un arbre\n";
		reponse += "1: Sapin : Bonne réponse\n";
		reponse += "2: Autre : Mauvaise réponse\n";
		reponse += "3: Autres2: Mauvaise réponse\n";
		reponse += "4: Autres3: Mauvaise réponse\n";
		
		assertEquals(reponse, attendu);
	}

	@Test
	void testImporterEnigmes() throws IOException, JSONException {
		ArrayList<Enigme> listeEnigmes = Enigme.importerEnigmes();
		assertEquals(listeEnigmes.size(), 8);
	}

	@Test
	void testGetQuestionFR() {
		assertEquals("Mon premier est un pronom possessif, mon second se trouve chez le boulanger, mon tout est un arbre", e.getQuestionFR());
	}

	@Test
	void testGetPropositionsFR() {
		ArrayList<Proposition> listePropositions = e.getPropositionsFR();
		ArrayList<Proposition> liste = new ArrayList<>();
		liste.add(new Proposition("Sapin : Bonne réponse", true));
		liste.add(new Proposition("Autre : Mauvaise réponse"));
		liste.add(new Proposition("Autres2: Mauvaise réponse"));
		liste.add(new Proposition("Autres3: Mauvaise réponse"));
		
		assertEquals(liste, listePropositions);
	}

}

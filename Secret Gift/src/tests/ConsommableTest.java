package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import equipement.Consommable;
import equipement.NumberConsommableException;
import jeu.Commande;

class ConsommableTest {
	
	private Consommable c;

	@BeforeEach
	void setUp() throws Exception {
		c = new Consommable(10, "Test", Commande.CODE, 1, 2);
	}

	@Test
	void testToString() {
		assertEquals(c.toString(), "Test -> C 1/2");
	}

	@Test
	void testConsommableIntStringCommandeInt() throws NumberConsommableException {
		Consommable c1 = new Consommable(11, "Test", Commande.AIDE, 1);
		assertTrue(c1 instanceof Consommable);
		assertThrows(NumberConsommableException.class, () -> new Consommable(11, "Test", Commande.AIDE, -1));
	}

	@Test
	void testConsommableIntStringCommande() {
		Consommable c1 = new Consommable(11, "Test", Commande.AIDE);
		assertTrue(c1 instanceof Consommable);
	}

	@Test
	void testConsommableIntStringCommandeIntInt() throws NumberConsommableException {
		Consommable c1 = new Consommable(11, "Test", Commande.AIDE, 1, 1);
		assertTrue(c1 instanceof Consommable);
		assertThrows(NumberConsommableException.class, () -> new Consommable(11, "Test", Commande.AIDE, -1));
		assertThrows(NumberConsommableException.class, () -> new Consommable(11, "Test", Commande.AIDE, 2, 1));
	}

	@Test
	void testGetMaxCapacite() {
		assertEquals(c.getMaxCapacite(), 2);
	}

	@Test
	void testGetDisponible() {
		assertEquals(c.getDisponible(), 1);
	}

	@Test
	void testEstDisponibleAndConsommeItem() {
		assertTrue(c.estDisponible());
		c.consommeItem();
		assertFalse(c.estDisponible());
	}

}

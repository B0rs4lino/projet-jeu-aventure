package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import carte.Proposition;

class PropositionTest {
	
	private Proposition propositionVraie;
	private Proposition propositionFausse;

	@BeforeEach
	void setUp() throws Exception {
		propositionVraie = new Proposition("Proposition 1", true);
		propositionFausse = new Proposition("Proposition 2", false);
	}

	@Test
	void testGetProposition() {
		assertEquals(propositionVraie.getProposition(), "Proposition 1");
		assertEquals(propositionFausse.getProposition(), "Proposition 2");
	}

	@Test
	void testIsJuste() {
		assertTrue(propositionVraie.isJuste());
		assertFalse(propositionFausse.isJuste());
	}

	@Test
	void testPropositionStringBoolean() {
		Proposition p = new Proposition("Prop", true);
		assertTrue(p instanceof Proposition);
		assertTrue(p.isJuste());
	}

	@Test
	void testPropositionString() {
		Proposition p = new Proposition("Prop");
		assertTrue(p instanceof Proposition);
		assertFalse(p.isJuste());
	}

	@Test
	void testToString() {
		assertEquals("Proposition 1", propositionVraie.toString());
		assertEquals("Proposition 2", propositionFausse.toString());
	}

}

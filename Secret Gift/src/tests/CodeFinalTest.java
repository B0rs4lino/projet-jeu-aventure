package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import equipement.CodeFinal;

class CodeFinalTest {
	
	private CodeFinal codeFinal;

	@BeforeEach
	void setUp() throws Exception {
		codeFinal = new CodeFinal("12");
	}

	@Test
	void testCodeFinal() {
		CodeFinal c = new CodeFinal();
		assertTrue(c instanceof CodeFinal);
	}

	@Test
	void testCodeFinalString() {
		CodeFinal c = new CodeFinal("12");
		assertTrue(c instanceof CodeFinal);
		assertEquals(c.getCode(), "12");
	}

	@Test
	void testAjouterNumeroAndGetCode() {
		codeFinal.ajouterNumero(2);
		assertEquals("122", codeFinal.getCode());
	}

	@Test
	void testCodeTrouveEntierement() {
		codeFinal.ajouterNumero(1);
		codeFinal.ajouterNumero(1);
		codeFinal.ajouterNumero(1);
		codeFinal.ajouterNumero(1);
		codeFinal.ajouterNumero(1);
		assertFalse(codeFinal.codeTrouveEntierement());
		codeFinal.ajouterNumero(1);
		assertTrue(codeFinal.codeTrouveEntierement());
	}

	@Test
	void testCodeTrouveMoitie() {
		codeFinal.ajouterNumero(1);
		assertFalse(codeFinal.codeTrouveMoitie());
		codeFinal.ajouterNumero(1);
		assertTrue(codeFinal.codeTrouveMoitie());
		
	}

	@Test
	void testRandomCode() {
		String r = CodeFinal.randomCode();
		assertEquals(r.length(), CodeFinal.LENGTH_CODE);
	}

}

package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import equipement.Item;
import jeu.Commande;

class ItemTest {
	
	private Item item;
	
	@BeforeEach
	void setUp() throws Exception {
		item = new Item(1, "Test", Commande.CODE);
	}

	@Test
	void testGetNom() {
		assertEquals(item.getNom(), "Test");
	}

	@Test
	void testGetAppel() {
		assertEquals(item.getAppel(), Commande.CODE);
	}

	@Test
	void testItem() {
		assertTrue(item instanceof Item);
	}

	@Test
	void testGetIdentifiant() {
		assertEquals(item.getIdentifiant(), 1);
	}

	@Test
	void testToString() {
		assertEquals(item.toString(), "Test -> " + Commande.CODE.getAbreviation());
	}

}

package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import jeu.Commande;
import jeu.Affichage.LANGUE;

class CommandeTest {

	@Test
	void testGetAbreviation() {
		assertEquals(Commande.AIDE.getAbreviation(), "?");
		assertEquals(Commande.TELEPORTATION.getAbreviation(), "TP");
	}

	@Test
	void testToString() {
		assertEquals(Commande.AIDE.toString(), "AIDE");
		assertEquals(Commande.TELEPORTATION.toString(), "TELEPORTATION");
	}

	@Test
	void testFindCommande() {
		assertEquals(Commande.findCommande("TP"), Commande.TELEPORTATION);
	}

	@Test
	void testToutesLesDescriptions() {
		assertEquals(Commande.toutesLesDescriptions().toString(), "[N (aller à la sortie NORD)\n"
				+ ", N (aller à la sortie SUD)\n"
				+ ", N (aller à la sortie EST)\n"
				+ ", N (aller à la sortie OUEST)\n"
				+ ", ESC (prendre l'escalier)\n"
				+ ", TP (Retourner à l'entrée de la maison)\n"
				+ ", ? (aide)\n"
				+ ", I (Lister l'inventaire)\n"
				+ ", EN (afficher enigme)\n"
				+ ", C (afficher code obtenu)\n"
				+ ", CA (ouvrir le cadeau final)\n"
				+ ", Q (quitter)\n"
				+ "]");
	}

	@Test
	void testToutesLesDescriptionsLANGUE() {
		assertEquals(Commande.toutesLesDescriptions(LANGUE.ENGLISH).toString(), "[N (Go to NORTH)\n"
				+ ", N (Go to SOUTH)\n"
				+ ", N (Go to EAST)\n"
				+ ", N (Go to WEST)\n"
				+ ", ESC (Take the stairs)\n"
				+ ", TP (Go back to starting area)\n"
				+ ", ? (help)\n"
				+ ", I (Inventory)\n"
				+ ", EN (Show riddle)\n"
				+ ", C (Show code found)\n"
				+ ", CA (Open the final gift)\n"
				+ ", Q (Quit)\n"
				+ "]");
		
		assertEquals(Commande.toutesLesDescriptions(LANGUE.FRANCAIS).toString(), "[N (aller à la sortie NORD)\n"
				+ ", N (aller à la sortie SUD)\n"
				+ ", N (aller à la sortie EST)\n"
				+ ", N (aller à la sortie OUEST)\n"
				+ ", ESC (prendre l'escalier)\n"
				+ ", TP (Retourner à l'entrée de la maison)\n"
				+ ", ? (aide)\n"
				+ ", I (Lister l'inventaire)\n"
				+ ", EN (afficher enigme)\n"
				+ ", C (afficher code obtenu)\n"
				+ ", CA (ouvrir le cadeau final)\n"
				+ ", Q (quitter)\n"
				+ "]");
		
	}

	@Test
	void testToutesLesAbreviations() {
		assertEquals(Commande.toutesLesAbreviations().toString(), "[N, S, E, O, ESC, TP, ?, I, EN, C, CA, Q]");
	}

	@Test
	void testTousLesNoms() {
		assertEquals(Commande.tousLesNoms().toString(), "[NORD, SUD, EST, OUEST, ESCALIER, TELEPORTATION, AIDE, INVENTAIRE, ENIGME, CODE, CADEAU, QUITTER]");
	}

}

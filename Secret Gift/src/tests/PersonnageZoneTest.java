package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import carte.Enigme;
import carte.PersonnageZone;
import carte.Proposition;
import jeu.Affichage.LANGUE;

class PersonnageZoneTest {
	
	private PersonnageZone personnage;

	@BeforeEach
	void setUp() throws Exception {
		ArrayList<Enigme> listeEnigme = new ArrayList<>();
		listeEnigme.add(new Enigme("Mon premier est le trou d’une aiguille, Mon second est un poisson et Mon tout est un bébé animal",
				new ArrayList<Proposition>(Arrays.asList(
						new Proposition("Autres3: Mauvaise réponse"),
						new Proposition("Autres2: Mauvaise réponse"),
						new Proposition("Chaton : Bonne réponse", true),
						new Proposition("Autre : Mauvaise réponse")
						))));
		listeEnigme.add(new Enigme("Je suis dans la cuisine, lorsqu’on me tourne la tête je pleure, qui suis-je ?",
				new ArrayList<Proposition>(Arrays.asList(
						new Proposition("Robinet : Bonne réponse", true),
						new Proposition("Autre : Mauvaise réponse")
						))));
		personnage = new PersonnageZone("Mère", new ArrayList<Enigme>(Arrays.asList(listeEnigme.get(0), listeEnigme.get(1))));
		System.out.println(listeEnigme.get(0).afficher(LANGUE.FRANCAIS));
	}

	@Test
	void testPersonnageZone() {
		assertTrue(personnage instanceof PersonnageZone);
	}

	@Test
	void testAfficherEnigme() {
		String voulu = "Mon premier est le trou d’une aiguille, Mon second est un poisson et Mon tout est un bébé animal\n"
				+ "1: Autres3: Mauvaise réponse\n"
				+ "2: Autres2: Mauvaise réponse\n"
				+ "3: Chaton : Bonne réponse\n"
				+ "4: Autre : Mauvaise réponse\n\nRéponse: \n";
		assertEquals(voulu, personnage.afficherEnigme(LANGUE.FRANCAIS));
	}

	@Test
	void testSetEnigmeResolue() {
		assertFalse(personnage.isEtage());
		personnage.setEnigmeResolue();
		assertTrue(personnage.isEtage());
	}

	@Test
	void testGetNom() {
		assertEquals(personnage.getNom(), "Mère");
	}

	@Test
	void testGetListeEnigmes() {
		ArrayList<Enigme> listeEnigme = new ArrayList<>();
		listeEnigme.add(new Enigme("Mon premier est le trou d’une aiguille, Mon second est un poisson et Mon tout est un bébé animal",
				new ArrayList<Proposition>(Arrays.asList(
						new Proposition("Autres3: Mauvaise réponse"),
						new Proposition("Autres2: Mauvaise réponse"),
						new Proposition("Chaton : Bonne réponse", true),
						new Proposition("Autre : Mauvaise réponse")
						))));
		listeEnigme.add(new Enigme("Je suis dans la cuisine, lorsqu’on me tourne la tête je pleure, qui suis-je ?",
				new ArrayList<Proposition>(Arrays.asList(
						new Proposition("Robinet : Bonne réponse", true),
						new Proposition("Autre : Mauvaise réponse")
						))));
		
		assertEquals(listeEnigme, personnage.getListeEnigmes());
	}

	@Test
	void testEnigmeCourante() {
		assertEquals(personnage.enigmeCourante(), new Enigme("Mon premier est le trou d’une aiguille, Mon second est un poisson et Mon tout est un bébé animal",
				new ArrayList<Proposition>(Arrays.asList(
						new Proposition("Autres3: Mauvaise réponse"),
						new Proposition("Autres2: Mauvaise réponse"),
						new Proposition("Chaton : Bonne réponse", true),
						new Proposition("Autre : Mauvaise réponse")
						))));
		personnage.setEnigmeResolue();;
		assertEquals(personnage.enigmeCourante(), new Enigme("Je suis dans la cuisine, lorsqu’on me tourne la tête je pleure, qui suis-je ?",
				new ArrayList<Proposition>(Arrays.asList(
						new Proposition("Robinet : Bonne réponse", true),
						new Proposition("Autre : Mauvaise réponse")
						))));
	}

	@Test
	void testVerifierReponse() {
		assertFalse(personnage.verifierReponse(1));
		assertTrue(personnage.verifierReponse(3));
	}

	@Test
	void testCreerPersonnageZone() throws IOException, JSONException, Exception {
		ArrayList<PersonnageZone> listePersonnage = PersonnageZone.creerPersonnageZone(Enigme.importerEnigmes());
		assertEquals(listePersonnage.size(), 4);
	}

}

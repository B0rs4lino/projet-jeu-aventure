package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import carte.Coffre;
import carte.Enigme;
import carte.PersonnageZone;
import carte.Proposition;
import carte.ZoneCoffre;
import jeu.Affichage.LANGUE;

class ZoneCoffreTest {
	
	private ZoneCoffre zoneCoffre;

	@BeforeEach
	void setUp() throws Exception {
		ArrayList<Enigme> listeEnigme = new ArrayList<>();
		listeEnigme.add(new Enigme("Mon premier est le trou d’une aiguille, Mon second est un poisson et Mon tout est un bébé animal",
				new ArrayList<Proposition>(Arrays.asList(
						new Proposition("Autres3: Mauvaise réponse"),
						new Proposition("Autres2: Mauvaise réponse"),
						new Proposition("Chaton : Bonne réponse", true),
						new Proposition("Autre : Mauvaise réponse")
						))));
		listeEnigme.add(new Enigme("Je suis dans la cuisine, lorsqu’on me tourne la tête je pleure, qui suis-je ?",
				new ArrayList<Proposition>(Arrays.asList(
						new Proposition("Robinet : Bonne réponse", true),
						new Proposition("Autre : Mauvaise réponse")
						))));
		PersonnageZone personnage = new PersonnageZone("Mère", new ArrayList<Enigme>(Arrays.asList(listeEnigme.get(0), listeEnigme.get(1))));
		zoneCoffre = new ZoneCoffre("description", "image", "imageDev", new Coffre(1), personnage);
	}

	@Test
	void testGetCoffre() {
		assertEquals(zoneCoffre.getCoffre(), new Coffre(1));
	}

	@Test
	void testZoneCoffreZoneStringCoffrePersonnageZoneLANGUE() {
		ArrayList<Enigme> listeEnigme = new ArrayList<>();
		listeEnigme.add(new Enigme("Mon premier est le trou d’une aiguille, Mon second est un poisson et Mon tout est un bébé animal",
				new ArrayList<Proposition>(Arrays.asList(
						new Proposition("Autres3: Mauvaise réponse"),
						new Proposition("Autres2: Mauvaise réponse"),
						new Proposition("Chaton : Bonne réponse", true),
						new Proposition("Autre : Mauvaise réponse")
						))));
		listeEnigme.add(new Enigme("Je suis dans la cuisine, lorsqu’on me tourne la tête je pleure, qui suis-je ?",
				new ArrayList<Proposition>(Arrays.asList(
						new Proposition("Robinet : Bonne réponse", true),
						new Proposition("Autre : Mauvaise réponse")
						))));
		ZoneCoffre zone = new ZoneCoffre("description", "image", "image", new Coffre(1),
				new PersonnageZone("Mère", new ArrayList<Enigme>(Arrays.asList(listeEnigme.get(0), listeEnigme.get(1)))));
		assertTrue(zone instanceof ZoneCoffre);
	}

	@Test
	void testVerifieEnigmeResolueAndSetEnigmeResolue() {
		assertFalse(zoneCoffre.verifieEnigmeResolue());
		zoneCoffre.setEnigmeResolue();
		assertTrue(zoneCoffre.verifieEnigmeResolue());
	}

	@Test
	void testAfficherEnigme() {
		String attendu = "Mon premier est le trou d’une aiguille, Mon second est un poisson et Mon tout est un bébé animal\n"
				+ "1: Autres3: Mauvaise réponse\n"
				+ "2: Autres2: Mauvaise réponse\n"
				+ "3: Chaton : Bonne réponse\n"
				+ "4: Autre : Mauvaise réponse\n"
				+ "\n"
				+ "Réponse: \n";
		assertEquals(attendu, zoneCoffre.afficherEnigme(LANGUE.FRANCAIS));
	}

	@Test
	void testVerifierReponseEnigme() {
		assertFalse(zoneCoffre.verifierReponseEnigme(1));
		assertTrue(zoneCoffre.verifierReponseEnigme(3));
		
	}

	@Test
	void testGetCodeCoffre() {
		assertEquals(zoneCoffre.getCodeCoffre(), new Coffre(1).getNumero());
	}

}
